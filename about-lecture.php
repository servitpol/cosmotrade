<?php
/**
 * Template file for lecture's page
 *
 * LICENSE:
 *
 * @category   Zend
 * @package    Zend_PageLecture
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    $Id:$
 * @link       http://framework.zend.com/package/PackageName
 * @since      File available since Release 1.5.0
 */
?>

<?php
/**
 * Template Name: Lecture
 * Template Post Type: post
 */
get_header(); ?>

  <div class="container video-head-container">
    <div class="video-head">
      <div class="video-head-left" style="background: url(<?php echo the_field("page-lecture-main-img");?>) no-repeat center / cover;">
        <div class="text-video-wrap">
          <h1><?php echo the_field("page-lecture-main-title");?></h1>
          <div class="trailer-desktop">
            <figure style="background: url(<?php echo the_field("page-lecture-main-videoimg");?>) no-repeat center / cover;">
              <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35 36"><path d="M17.5 36C7.836 36 0 27.94 0 18S7.836 0 17.5 0 35 8.06 35 18s-7.836 18-17.5 18zM13.125 9.014v17.973L26.25 18 13.125 9.014z" fill="#fff" fill-opacity=".56"/></svg>
            </figure>
            <div class="text">
              <span>—</span>смотреть <br>трейлер
            </div>
          </div>
        </div>
        <div class="video-wrap">
          <svg class="btn-video-wrap-close" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26"><path d="M24.375 24.375L1.625 1.625m0 22.75l22.75-22.75-22.75 22.75z" stroke="#3E4145" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path></svg>
          <div class="video-handler">
            <iframe src="https://www.youtube.com/embed/<?php echo the_field("page-lecture-main-video");?>?rel=0&amp;showinfo=0" width="300" height="150" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
          </div>
        </div>
      </div>
      <div class="video-head-right">
        <p class="chr-title">Цена</p>
        <p class="chr-content"><?php echo the_field("page-lecture-main-price");?></p>
        <p class="chr-title">Длительность</p>
        <p class="chr-content"><?php echo the_field("page-lecture-main-time");?></p>
        <p class="chr-title">Преподаватель</p>
        <p class="chr-content"><?php echo the_field("page-lecture-main-teacher");?></p>
        <button class="call-lecture">Приобрести</button>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="video-about">
      <h2>О лекции</h2>
      <div class="row">
        <div class="col-12 col-md-8">
          <article>
            <p><?php echo the_field("page-lecture-info-text");?></p>
          </article>
          <button class="call-lecture">Приобрести</button>
        </div>
        <div class="col-12 col-md-4">
          <p>Скидка на покупку оборудования</p>
          <p>-<?php echo the_field("page-lecture-info-sale");?></p>
          <p>Свободных мест осталось</p>
          <p><?php echo the_field("page-lecture-info-queue");?></p>
        </div>
      </div>
    </div>
  </div>

<?php include("template-parts/content-howteach.php");?>

<?php
get_footer();