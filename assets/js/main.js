$(".btn-menu").click(function(){
    $("header").addClass("active");
    $(".header-mob").addClass("active");
    $("body").addClass("noscroll");
});

$(".btn-menu-close").click(function(){
    $("header").removeClass("active");
    $(".header-mob").removeClass("active");
    $("body").removeClass("noscroll");
});

$(".why-more").click(function(){
  $(".block-why").addClass("active");
});

if($(window).width() < 767) {
  $(".menu-item-has-children").click(function(){
    $(this).toggleClass("show-submenu");
    return false;
  });
}

$(".lc-holder").click(function(){
  $(".lang-change").toggleClass("deactivated");
});

$("#regPhone").mask("+38 (099) 999-99-99").on('click', function(){
  $(this).unmask();
  $(this).mask("+38 (099) 999-99-99");
  $(this).focus();
});

$("#signPhone").mask("+38 (099) 999-99-99").on('click', function(){
  $(this).unmask();
  $(this).mask("+38 (099) 999-99-99");
  $(this).focus();
});

$(".call-login").click(function(){
  $("body").addClass("noscroll");
  $(".modal-shadow").addClass("active");
  $(".modal-wrap-login").addClass("active");
  $("header").removeClass("active");
  $(".header-mob").removeClass("active");
});

$(".btn-login-close").click(function(){
  $("body").removeClass("noscroll");
  $(".modal-shadow").removeClass("active");
  $(".modal-wrap-login").removeClass("active");
});

$(".call-reg").click(function(){
  $("body").addClass("noscroll");
  $(".modal-shadow").addClass("active");
  $(".modal-wrap-reg").addClass("active");
  $("header").removeClass("active");
  $(".header-mob").removeClass("active");
});

$(".btn-reg-close").click(function(){
  $("body").removeClass("noscroll");
  $(".modal-shadow").removeClass("active");
  $(".modal-wrap-reg").removeClass("active");
});

$(".call-sign").click(function(){
  $("body").addClass("noscroll");
  $(".modal-shadow").addClass("active");
  $(".modal-wrap-sign").addClass("active");
  $("header").removeClass("active");
  $(".header-mob").removeClass("active");
});

$(".btn-sign-close").click(function(){
  $("body").removeClass("noscroll");
  $(".modal-shadow").removeClass("active");
  $(".modal-wrap-sign").removeClass("active");
});

$(".call-lecture").click(function(){
  $("body").addClass("noscroll");
  $(".modal-shadow").addClass("active");
  $(".modal-wrap-lecture").addClass("active");
  $("header").removeClass("active");
  $(".header-mob").removeClass("active");
});

$(".btn-lecture-close").click(function(){
  $("body").removeClass("noscroll");
  $(".modal-shadow").removeClass("active");
  $(".modal-wrap-lecture").removeClass("active");
});

$(".modal-wrap-login .call-reg").click(function(){
    $(".modal-wrap-login").removeClass("active");
});

$(".modal-wrap-reg .call-login").click(function(){
    $(".modal-wrap-reg").removeClass("active");
});

$('.why-slider').slick({
  dots: false,
  infinite: false,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1245,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 767,
      settings: "unslick",
    }
  ]
});

$('.who-slider-text').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.who-slider-img',
  infinite: false,
  responsive: [
    {
      breakpoint: 767,
      settings: {
        arrows: true,
      }
    }
  ]
});

$('.who-slider-img').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  asNavFor: '.who-slider-text',
  fade: true,
  infinite: false,
  responsive: [
    {
      breakpoint: 767,
      settings: {
        arrows: false,
        dots: true
      }
    }
  ]
});

$('.bci-content').slick({
  infinite: true,
  arrows: false,
  dots: true,
  slidesToShow: 1,
  autoplay: true,
  slidesToScroll: 1
});

$(".page-template-courses .courses-category-item").click(function(){
  var courseCategory = $(this).attr('class').split(' ')[1];
  $(".courses-category-item").removeClass("active");
  $(this).addClass("active");
  $(".courses-item").not("."+ courseCategory).removeClass("show");
  $(".courses-item."+ courseCategory).addClass("show");
});

$( "#coursesTime" ).selectmenu({
  change: function(event, ui) {
    var courseTime = $(this).val();
    $('.courses-item').not('.'+ courseTime).removeClass("show");
    $('.'+ courseTime).addClass("show");
  }
});

$( "#coursesCity" ).selectmenu({
  change: function(event, ui) {
    var coursesCity = $(this).val();
    $('.courses-item').not('.'+ coursesCity).removeClass("show");
    $('.'+ coursesCity).addClass("show");
  }
});

$(".page-template-teachers .courses-category-item").click(function(){
  var courseCategory = $(this).attr('class').split(' ')[1];
  $(".courses-category-item").removeClass("active");
  $(this).addClass("active");
  $(".teachers-item").not("."+ courseCategory).removeClass("show");
  $(".teachers-item."+ courseCategory).addClass("show");
});

$(".page-template-videos .courses-category-item").click(function(){
  var courseCategory = $(this).attr('class').split(' ')[1];
  $(".courses-category-item").removeClass("active");
  $(this).addClass("active");
  $(".lecture-item").not("."+ courseCategory).removeClass("show");
  $(".lecture-item."+ courseCategory).addClass("show");
});

$(".page-template-blog .courses-category-item").click(function(){
  var courseCategory = $(this).attr('class').split(' ')[1];
  $(".courses-category-item").removeClass("active");
  $(this).addClass("active");
  $(".blog-item").not("."+ courseCategory).removeClass("show");
  $(".blog-item."+ courseCategory).addClass("show");
});

$( "#coursesType" ).selectmenu();
$( "#blogSort" ).selectmenu();


if($(window).width() < 767) {
  $('.course-advantages .row').slick({
    infinite: true,
    arrows: false,
    dots: true,
    slidesToShow: 2,
    autoplay: true,
    slidesToScroll: 1
  });
}

$('.block-course-coming .row').slick({
  infinite: true,
  arrows: false,
  dots: false,
  slidesToShow: 3,
  autoplay: true,
  slidesToScroll: 1
});

$('.block-teacher .row').slick({
  infinite: true,
  arrows: false,
  dots: false,
  slidesToShow: 3,
  autoplay: true,
  slidesToScroll: 1
});

$(".course-program-item .cpi-text .title, .course-program-item .cpi-plus, .course-program-item .cpi-minus").click(function(){
  $(this).closest(".course-program-item").toggleClass("show");
});

$(".home .heading-block .trailer-desktop figure, .heading-block .trailer-mobile").click(function(){
  $(".video-wrap").addClass("show");
});

$(".home .video-wrap .btn-video-wrap-close").click(function(){
  $(".video-wrap").removeClass("show");
});

$(".post-template-about-course .course-head-container .trailer-desktop figure").click(function(){
  $(".video-wrap").addClass("show");
});

$(".post-template-about-course .video-wrap .btn-video-wrap-close").click(function(){
  $(".video-wrap").removeClass("show");
});

$('.teacher-course-slider').slick({
  infinite: true,
  arrows: true,
  dots: false,
  slidesToShow: 1,
  autoplay: true,
  slidesToScroll: 1
});

$('.block-teacher-works .row').slick({
  infinite: true,
  arrows: true,
  dots: false,
  slidesToShow: 3,
  autoplay: false,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }
  ]
});

$(".video-head-container .trailer-desktop figure").click(function(){
  $(".video-wrap").addClass("show");
});

$(".video-wrap .btn-video-wrap-close").click(function(){
  $(".video-wrap").removeClass("show");
});

$('.similar-article .row').slick({
  infinite: true,
  arrows: true,
  dots: false,
  slidesToShow: 3,
  autoplay: false,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

$('.course-reviews .row').slick({
  infinite: true,
  arrows: true,
  dots: false,
  slidesToShow: 3,
  autoplay: false,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }
  ]
});

$(".btn-city.kiev").click(function(){
  $(".btn-city").removeClass("active");
  $(".map-wrap").removeClass("active");
  $(this).addClass("active");
  $(".map-wrap.kiev").addClass("active");
});

$(".btn-city.odessa").click(function(){
  $(".btn-city").removeClass("active");
  $(".map-wrap").removeClass("active");
  $(this).addClass("active");
  $(".map-wrap.odessa").addClass("active");
});

$( "#vacancyCity" ).selectmenu();
$( "#vacancyEmployment" ).selectmenu();

$(".call-vacancyAdd").click(function(){
  $("body").addClass("noscroll");
  $(".modal-shadow").addClass("active");
  $(".modal-wrap-vacancyAdd").addClass("active");
  $("header").removeClass("active");
  $(".header-mob").removeClass("active");
});

$(".btn-vacancyAdd-close, .btn-vacancyAdd-send").click(function(){
  $("body").removeClass("noscroll");
  $(".modal-shadow").removeClass("active");
  $(".modal-wrap-vacancyAdd").removeClass("active");
});

$( "#resumeWriteBirth" ).selectmenu();

$("#resumeWritePhone").mask("+38 (099) 999-99-99").on('click', function(){
  $(this).unmask();
  $(this).mask("+38 (099) 999-99-99");
  $(this).focus();
});

$( "#resumeWriteCity" ).selectmenu();
$( "#resumeWriteEducationMonthFrom" ).selectmenu();
$( "#resumeWriteEducationYearFrom" ).selectmenu();
$( "#resumeWriteEducationMonthTo" ).selectmenu();
$( "#resumeWriteEducationYearTo" ).selectmenu();
$( "#resumeWriteWorkMonthFrom" ).selectmenu();
$( "#resumeWriteWorkYearFrom" ).selectmenu();
$( "#resumeWriteWorkMonthTo" ).selectmenu();
$( "#resumeWriteWorkYearTo" ).selectmenu();

$(".call-resumeAdd").click(function(){
  $("body").addClass("noscroll");
  $(".modal-shadow").addClass("active");
  $(".modal-wrap-resumeAdd").addClass("active");
  $("header").removeClass("active");
  $(".header-mob").removeClass("active");
});

$(".btn-resumeAdd-close").click(function(){
  $("body").removeClass("noscroll");
  $(".modal-shadow").removeClass("active");
  $(".modal-wrap-resumeAdd").removeClass("active");
});

$("#resumeUploadPhone").mask("+38 (099) 999-99-99").on('click', function(){
  $(this).unmask();
  $(this).mask("+38 (099) 999-99-99");
  $(this).focus();
});

function resumeImg(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $('#resumePhotoHolder')
          .css('background-image', 'url('+ e.target.result +')');
    };

    reader.readAsDataURL(input.files[0]);
  }
}

$(".resume-tabs .resumeWrite").click(function(){
  $(".resume-tabs p").removeClass("active");
  $(".resume-tab").removeClass("active");
  $(this).addClass("active");
  $(".resume-tab-write").addClass("active");
});

$(".resume-tabs .resumeUpload").click(function(){
  $(".resume-tabs p").removeClass("active");
  $(".resume-tab").removeClass("active");
  $(this).addClass("active");
  $(".resume-tab-upload").addClass("active");
});

$(".call-review").click(function(){
  $("body").addClass("noscroll");
  $(".modal-shadow").addClass("active");
  $(".modal-wrap-review").addClass("active");
  $("header").removeClass("active");
  $(".header-mob").removeClass("active");
});

$(".btn-review-close").click(function(){
  $("body").removeClass("noscroll");
  $(".modal-shadow").removeClass("active");
  $(".modal-wrap-review").removeClass("active");
});

$( "#reviewCategory" ).selectmenu();

var input = document.getElementById( 'resumeFile' );
var infoArea = document.getElementById( 'resumeFileName' );

input.addEventListener( 'change', showFileName );

function showFileName( event ) {
  var input = event.srcElement;
  var fileName = input.files[0].name;
  infoArea.textContent = fileName;
}