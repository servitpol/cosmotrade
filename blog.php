<?php
/**
 * Template file for blog page
 *
 * LICENSE:
 *
 * @category   Zend
 * @package    Zend_PageBlog
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    $Id:$
 * @link       http://framework.zend.com/package/PackageName
 * @since      File available since Release 1.5.0
 */
?>

<?php
/**
 * Template Name: Blog
 */
get_header(); ?>

<?php include("template-parts/content-courses.php");?>

  <div class="container">
    <h1>Блог</h1>
    <div class="courses-category">
      <div class="courses-category-item blog-all active">Все статьи</div>
      <div class="courses-category-item cosmetology-blog">Косметология</div>
      <div class="courses-category-item injection-techniques-blog">Инъекционные методики</div>
      <div class="courses-category-item medical-blog">Медицинские курсы</div>
      <div class="courses-category-item permanent-blog">Перманентный макияж</div>
      <div class="courses-category-item trichology-blog">Трихология</div>
      <div class="courses-category-item laser-hair-removal-blog">Лазерная эпиляция</div>
      <div class="courses-category-item depilation-blog">Депиляция</div>
      <div class="courses-category-item podology-blog">Подология</div>
      <div class="courses-category-item business-blog">Бизнес</div>
    </div>
    <div class="courses-filter">
      <div class="courses-select">
        <label for="blogSort">Сортировать</label>
        <select name="blogSort" id="blogSort">
          <option>Сначала новые</option>
          <option>Сначала старые</option>
        </select>
      </div>
    </div>

    <div class="blog-list">
      <div class="row">
        <? $args = array(
            'post_type' => 'post',
            'category_name'=>'blog',
            'posts_per_page' => 12,
        );
        $loop = new wp_Query($args);
        while($loop->have_posts()) : $loop->the_post();?>
          <?php
          $categories = get_the_category();
          $cls = '';

          if ( ! empty( $categories ) ) {
            foreach ( $categories as $cat ) {
              $cls .= $cat->slug . ' ';
            }
          }
          ?>
        <div class="col-12 col-sm-6 col-lg-4 blog-all show blog-item <?php echo $cls; ?>">
          <a href="<?php the_permalink(); ?>">
            <figure style="background: url(<?php echo get_the_post_thumbnail_url(); ?>) no-repeat center / cover;"></figure>
            <p class="date"><?php the_date('j F Y'); ?></p>
            <p class="title"><?php echo the_field("page-article-title")?></p>
            <p class="description"><?php the_excerpt(); ?></p>
          </a>
        </div>
        <? endwhile;?>
        <? wp_reset_query(); ?>
      </div>
      <div class="blog-more">
        Показать ещё
        <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15"><path d="M14.124 7.051L7.114.967A.298.298 0 006.92.895H5.277c-.137 0-.2.17-.096.26l6.497 5.64H.82a.149.149 0 00-.148.148v1.114c0 .081.067.148.148.148h10.857l-6.498 5.64c-.104.092-.041.26.096.26h1.698c.035 0 .07-.013.097-.037l7.054-6.119a.594.594 0 000-.898z" fill="#73A7F0"/></svg>
      </div>
      <hr>
    </div>

  </div>

<?php
get_footer();