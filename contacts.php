<?php
/**
 * Template file for contacts page
 *
 * LICENSE:
 *
 * @category   Zend
 * @package    Zend_PageContacts
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    $Id:$
 * @link       http://framework.zend.com/package/PackageName
 * @since      File available since Release 1.5.0
 */
?>

<?php
/**
 * Template Name: Contacts
 */
get_header(); ?>


  <div class="contacts-page-wrap">
    <div class="container">
      <h1><?php echo the_field("page-contacts-title")?></h1>
      <div class="map-wrap kiev active">
        <div id="map1"></div>
      </div>
      <div class="map-wrap odessa">
        <div id="map2"></div>
      </div>
      <div class="row">
        <div class="col-12 col-md-4 col-lg-3">
          <p class="btn-city kiev active"><?php echo the_field("page-contacts-kiev-city")?></p>
          <p class="street"><?php echo the_field("page-contacts-kiev-street")?></p>
          <p class="phone"><a href="tel:<?php echo the_field("page-contacts-kiev-phone-1-link")?>"><?php echo the_field("page-contacts-kiev-phone-1")?></a></p>
          <p class="phone"><a href="tel:<?php echo the_field("page-contacts-kiev-phone-2-link")?>"><?php echo the_field("page-contacts-kiev-phone-2")?></a></p>
          <p class="mail"><a href="mailto:<?php echo the_field("page-contacts-kiev-email")?>"><?php echo the_field("page-contacts-kiev-email")?></a></p>
        </div>
        <div class="col-12 col-md-4 col-lg-3">
          <p class="btn-city odessa"><?php echo the_field("page-contacts-odessa-city")?></p>
          <p class="street"><?php echo the_field("page-contacts-odessa-street")?></p>
          <p class="phone"><a href="tel:<?php echo the_field("page-contacts-odessa-phone-1-link")?>"><?php echo the_field("page-contacts-odessa-phone-1")?></a></p>
          <p class="phone"><a href="tel:<?php echo the_field("page-contacts-odessa-phone-2-link")?>"><?php echo the_field("page-contacts-odessa-phone-2")?></a></p>
          <p class="mail"><a href="mailto:<?php echo the_field("page-contacts-odessa-email")?>"><?php echo the_field("page-contacts-odessa-email")?></a></p>
        </div>
        <div class="col-12 col-md-4 col-lg-3">
          <p class="contacts-title"><?php echo the_field("page-contacts-soc")?></p>
          <div class="contacts-soc-row">
            <a href="<?php echo the_field("page-contacts-fb")?>" class="bt-fb"><svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 36 36"><g clip-path="url(#clip0)"><path d="M17.996.72C8.455.72.721 8.454.721 17.995c0 9.541 7.734 17.275 17.275 17.275 9.541 0 17.275-7.734 17.275-17.275C35.271 8.454 27.537.72 17.996.72zm4.092 11.938h-2.597c-.307 0-.65.405-.65.943v1.875h3.249l-.491 2.674h-2.757v8.027h-3.065V18.15h-2.78v-2.674h2.78v-1.573c0-2.257 1.566-4.09 3.714-4.09h2.597v2.845z" fill="#FFA4E5"/></g><defs><clipPath id="clip0"><path fill="#fff" d="M0 0h35.99v35.99H0z"/></clipPath></defs></svg></a>
            <a href="<?php echo the_field("page-contacts-ins")?>" class="bt-ig"><svg width="18" height="16.42" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18"><path d="M9 .987c-2.447 0-2.752.01-3.709.048-.961.043-1.612.179-2.186.382-.6.207-1.144.53-1.593.949-.46.409-.815.905-1.04 1.453C.248 4.343.1 4.936.051 5.813.01 6.687 0 6.964 0 9.196c0 2.231.012 2.51.052 3.382.048.875.197 1.47.42 1.994a4.013 4.013 0 001.04 1.453c.448.42.992.744 1.593.95.574.2 1.227.338 2.186.381.958.04 1.262.048 3.709.048s2.752-.01 3.709-.048c.959-.043 1.612-.182 2.186-.382.6-.207 1.144-.53 1.593-.949.46-.408.816-.904 1.04-1.453.22-.524.372-1.12.42-1.994.043-.873.052-1.151.052-3.382 0-2.232-.012-2.51-.052-3.383-.048-.875-.2-1.473-.42-1.994a4.013 4.013 0 00-1.04-1.453 4.4 4.4 0 00-1.593-.949c-.574-.203-1.227-.34-2.186-.382C11.751.995 11.447.987 9 .987zm0 1.478c2.403 0 2.69.01 3.64.047.875.038 1.352.171 1.669.284.392.132.746.342 1.037.614.299.265.528.588.67.944.125.289.271.724.312 1.523.04.866.052 1.128.052 3.32 0 2.19-.011 2.452-.055 3.318-.048.8-.194 1.234-.317 1.523-.17.386-.36.655-.675.946-.292.271-.646.48-1.037.612-.313.113-.797.246-1.676.284-.954.037-1.236.047-3.645.047s-2.69-.01-3.645-.05c-.876-.043-1.36-.177-1.676-.29a2.863 2.863 0 01-1.035-.614 2.456 2.456 0 01-.673-.947c-.126-.285-.27-.726-.317-1.528-.032-.86-.047-1.127-.047-3.314 0-2.186.015-2.453.047-3.325.047-.802.19-1.242.317-1.528.155-.39.357-.657.673-.946.314-.286.607-.47 1.035-.615.317-.113.788-.246 1.667-.286.955-.032 1.236-.043 3.642-.043L9 2.465zm0 2.518c-.607 0-1.208.108-1.768.32-.561.212-1.07.522-1.5.913a4.21 4.21 0 00-1.001 1.368 3.893 3.893 0 00-.351 1.613c0 .553.119 1.101.35 1.613a4.21 4.21 0 001.002 1.367c.43.391.939.702 1.5.913.56.212 1.161.32 1.768.32s1.208-.108 1.768-.32a4.67 4.67 0 001.5-.913c.429-.391.769-.856 1.001-1.367a3.892 3.892 0 000-3.226 4.209 4.209 0 00-1.001-1.368 4.671 4.671 0 00-1.5-.913A5.008 5.008 0 009 4.983zm0 6.95c-1.658 0-3-1.224-3-2.736C6 7.684 7.342 6.46 9 6.46c1.659 0 3 1.224 3 2.737 0 1.512-1.341 2.736-3 2.736zm5.887-7.119a.945.945 0 01-.318.697 1.137 1.137 0 01-.764.289 1.17 1.17 0 01-.413-.075 1.092 1.092 0 01-.35-.213.985.985 0 01-.235-.32.91.91 0 010-.754.985.985 0 01.234-.319c.1-.091.22-.164.35-.213a1.17 1.17 0 01.414-.075c.595 0 1.082.44 1.082.983z" fill="#fff"></path></svg></a>
          </div>
          <a href="mailto:<?php echo the_field("page-contacts-email")?>" class="contacts-write">
            <?php echo the_field("page-contacts-btn")?>
            <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15"><path d="M14.124 7.051L7.114.967A.298.298 0 006.92.895H5.277c-.137 0-.2.17-.096.26l6.497 5.64H.82a.149.149 0 00-.148.148v1.114c0 .081.067.148.148.148h10.857l-6.498 5.64c-.104.092-.041.26.096.26h1.698c.035 0 .07-.013.097-.037l7.054-6.119a.594.594 0 000-.898z" fill="#73A7F0"/></svg>
          </a>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <hr class="contacts-hr">
  </div>

  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDrXdjgit4q-Yvrh1WH14JAGGIMQ4PmWwQ"></script>

  <script>
    google.maps.event.addDomListener(window, "load", init1);
    function init1() {
      var mapOptions = {
        zoom: 15,
        center: new google.maps.LatLng(<?php echo the_field("page-contacts-kiev-mapcenter")?>),
        scrollwheel: false,
        navigationControl: false,
        mapTypeControl: false,
        styles: [
          { featureType: "administrative", elementType: "labels.text.fill", stylers: [{ color: "#444444" }] },
          { featureType: "landscape", elementType: "all", stylers: [{ color: "#f2f2f2" }] },
          { featureType: "poi", elementType: "all", stylers: [{ visibility: "off" }] },
          { featureType: "road", elementType: "all", stylers: [{ saturation: -100 }, { lightness: 45 }] },
          { featureType: "road.highway", elementType: "all", stylers: [{ visibility: "simplified" }] },
          { featureType: "road.arterial", elementType: "labels.icon", stylers: [{ visibility: "off" }] },
          { featureType: "transit", elementType: "all", stylers: [{ visibility: "off" }] },
          { featureType: "water", elementType: "all", stylers: [{ color: "#46bcec" }, { visibility: "on" }] },
        ],
      };
      var mapElement = document.getElementById("map1");
      var image = "<?php echo get_bloginfo('template_url'); ?>/assets/img//map-icon.png";
      var map = new google.maps.Map(mapElement, mapOptions);
      var marker = new google.maps.Marker({ position: new google.maps.LatLng(<?php echo the_field("page-contacts-kiev-maplabel")?>), map: map, icon: image, title: "КОСМОТРЕЙД" });
    }

    google.maps.event.addDomListener(window, "load", init2);
    function init2() {
      var mapOptions = {
        zoom: 15,
        center: new google.maps.LatLng(<?php echo the_field("page-contacts-odessa-mapcenter")?>),
        scrollwheel: false,
        navigationControl: false,
        mapTypeControl: false,
        styles: [
          { featureType: "administrative", elementType: "labels.text.fill", stylers: [{ color: "#444444" }] },
          { featureType: "landscape", elementType: "all", stylers: [{ color: "#f2f2f2" }] },
          { featureType: "poi", elementType: "all", stylers: [{ visibility: "off" }] },
          { featureType: "road", elementType: "all", stylers: [{ saturation: -100 }, { lightness: 45 }] },
          { featureType: "road.highway", elementType: "all", stylers: [{ visibility: "simplified" }] },
          { featureType: "road.arterial", elementType: "labels.icon", stylers: [{ visibility: "off" }] },
          { featureType: "transit", elementType: "all", stylers: [{ visibility: "off" }] },
          { featureType: "water", elementType: "all", stylers: [{ color: "#46bcec" }, { visibility: "on" }] },
        ],
      };
      var mapElement = document.getElementById("map2");
      var image = "<?php echo get_bloginfo('template_url'); ?>/assets/img//map-icon.png";
      var map = new google.maps.Map(mapElement, mapOptions);
      var marker = new google.maps.Marker({ position: new google.maps.LatLng(<?php echo the_field("page-contacts-odessa-maplabel")?>), map: map, icon: image, title: "КОСМОТРЕЙД" });
    }
  </script>

<?php
get_footer();