<?php
/**
 * Template file for course catalog page
 *
 * LICENSE:
 *
 * @category   Zend
 * @package    Zend_PageCourses
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    $Id:$
 * @link       http://framework.zend.com/package/PackageName
 * @since      File available since Release 1.5.0
 */
?>

<?php
/**
 * Template Name: Courses
 */
get_header(); ?>

  <main>

    <?php include("template-parts/content-courses.php");?>

    <div class="container">
      <h1><?php echo the_field("page-courses-title")?></h1>
      <div class="courses-filter">
        <div class="courses-search">
          <input placeholder="Поиск по ключевым словам" type="text">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 515.558 515.558"><path d="M378.344 332.78c25.37-34.645 40.545-77.2 40.545-123.333C418.889 93.963 324.928.002 209.444.002S0 93.963 0 209.447s93.961 209.445 209.445 209.445c46.133 0 88.692-15.177 123.337-40.547l137.212 137.212 45.564-45.564L378.344 332.78zm-168.899 21.667c-79.958 0-145-65.042-145-145s65.042-145 145-145 145 65.042 145 145-65.043 145-145 145z"/></svg>
        </div>
        <div class="courses-select">
          <label for="coursesTime">Форма обучения</label>
          <select name="coursesTime" id="coursesTime">
            <option value="filter-any">Любая</option>
            <option value="filter-daily">Дневная</option>
            <option value="filter-evening">Вечерняя</option>
            <option value="filter-holiday">Выходного дня</option>
            <option value="filter-distance">Дистанционная</option>
          </select>
        </div>
        <div class="courses-select">
          <label for="coursesCity">Город</label>
          <select name="coursesCity" id="coursesCity">
            <option value="filter-all">Все</option>
            <option value="filter-kiev">Киев</option>
            <option value="filter-odessa">Одесса</option>
          </select>
        </div>
      </div>
      <div class="courses-category">
        <div class="courses-category-item category-all active">Все курсы</div>
        <div class="courses-category-item cosmetology">Косметология</div>
        <div class="courses-category-item injection-techniques">Инъекционные методики</div>
        <div class="courses-category-item medical">Медицинские курсы</div>
        <div class="courses-category-item permanent">Перманентный макияж</div>
        <div class="courses-category-item trichology">Трихология</div>
        <div class="courses-category-item laser-hair-removal">Лазерная эпиляция</div>
        <div class="courses-category-item depilation">Депиляция</div>
        <div class="courses-category-item podology">Подология</div>
        <div class="courses-category-item business">Бизнес</div>
      </div>
      <div class="courses-list">
        <? $args = array(
            'post_type' => 'post',
            'category_name'=>'courses',
            'posts_per_page' => 5,
        );
        $loop = new wp_Query($args);
        while($loop->have_posts()) : $loop->the_post();?>
        <?php
        $categories = get_the_category();
        $cls = '';

        if ( ! empty( $categories ) ) {
          foreach ( $categories as $cat ) {
            $cls .= $cat->slug . ' ';
          }
        }
        ?>
        <div class="courses-item show <?php echo $cls; ?> <?php echo the_field("page-course-block1-city")?> <?php echo the_field("page-course-courses-format")?> filter-all filter-any category-all">
          <figure style="background: url(<?php echo the_field('page-course-courses-img');?>) no-repeat center / cover;">
            <div class="star-wrap">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -10 511.991 511"><path d="M510.652 185.883a27.177 27.177 0 00-23.402-18.688l-147.797-13.418-58.41-136.75C276.73 6.98 266.918.497 255.996.497s-20.738 6.483-25.023 16.53l-58.41 136.75-147.82 13.418c-10.837 1-20.013 8.34-23.403 18.688a27.25 27.25 0 007.937 28.926L121 312.773 88.059 457.86c-2.41 10.668 1.73 21.7 10.582 28.098a27.087 27.087 0 0015.957 5.184 27.14 27.14 0 0013.953-3.86l127.445-76.203 127.422 76.203a27.197 27.197 0 0029.934-1.324c8.851-6.398 12.992-17.43 10.582-28.098l-32.942-145.086 111.723-97.964a27.246 27.246 0 007.937-28.926zM258.45 409.605"/></svg>
              <?php echo the_field("page-course-block1-rating-count")?>
            </div>
            <div class="queue-left"><?php echo the_field("page-course-info-queue");?> мест осталось</div>
          </figure>
          <div class="courses-item-content">
            <div class="cic-cfg">
              <div class="cic-cfg-item">
                <svg width="16" height="13.49" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 14"><path d="M13.554 1.024h-.156V.797a.538.538 0 00-.532-.544h-1.242a.544.544 0 00-.54.544v.227H4.917V.797a.553.553 0 00-.55-.544H3.124a.55.55 0 00-.546.544v.227h-.142A2.445 2.445 0 000 3.472v7.824c0 1.347 1.09 2.451 2.436 2.451h11.118A2.456 2.456 0 0016 11.297V3.471c0-1.346-1.1-2.448-2.446-2.448zm.301 10.03c0 .35-.294.645-.644.645H2.779a.644.644 0 01-.634-.645v-7.34c0-.35.285-.642.634-.642h10.432a.65.65 0 01.644.642v7.34z" fill="#73A7F0"/><path d="M7.875 4.301H6.56a.535.535 0 00-.535.535v1.316c0 .296.24.535.535.535h1.316c.295 0 .534-.24.534-.535V4.836a.534.534 0 00-.534-.535zM11.971 4.301h-1.316a.534.534 0 00-.534.535v1.316c0 .296.24.535.534.535h1.316c.295 0 .535-.24.535-.535V4.836a.535.535 0 00-.535-.535zM11.971 8.036h-1.316a.534.534 0 00-.534.535v1.316c0 .295.24.534.534.534h1.316c.295 0 .535-.24.535-.534V8.571a.535.535 0 00-.535-.535z" fill="#73A7F0"/></svg>
                <?php echo the_field("page-course-courses-date")?>
              </div>
              <div class="cic-cfg-item">
                <svg width="15.22" height="16" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16"><path d="M10.757 7.2h-1.69V5.165a1.066 1.066 0 10-2.132 0v3.103A1.066 1.066 0 008 9.336h2.758a1.067 1.067 0 10-.002-2.136z" fill="#73A7F0"/><path d="M13.19 13.828a7.588 7.588 0 002.42-5.56c0-1.452-.41-2.81-1.117-3.965l.484-.601c.609-.757.226-2.076-.856-2.947L14.077.72c-.52-.418-1.134-.675-1.708-.713-.575-.04-1.06.143-1.354.506l-.47.585A7.578 7.578 0 008 .659c-.895 0-1.753.156-2.551.44L4.975.512c-.292-.364-.78-.546-1.353-.507-.574.039-1.19.296-1.708.714L1.87.755c-1.082.871-1.465 2.19-.856 2.947l.49.608A7.566 7.566 0 00.391 8.268c0 2.192.932 4.17 2.42 5.56l-.555.756a.89.89 0 101.434 1.053l.554-.754A7.56 7.56 0 008 15.877a7.56 7.56 0 003.757-.994l.553.754a.888.888 0 001.244.19.89.89 0 00.19-1.243l-.555-.756zM2.512 8.268A5.493 5.493 0 018 2.781a5.493 5.493 0 015.487 5.487A5.493 5.493 0 018 13.755a5.493 5.493 0 01-5.487-5.487z" fill="#73A7F0"/></svg>
                <?php echo the_field("page-course-courses-time");?>
              </div>
              <div class="cic-cfg-item">
                <svg width="14.84" height="16" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16"><path d="M9.718 1.36V.756A.762.762 0 008.96 0H7.031a.747.747 0 00-.74.756v.604A7.42 7.42 0 007.998 16c4.091 0 7.423-3.33 7.423-7.42 0-3.501-2.442-6.444-5.703-7.22zm-1.15 12.325c-.145.017-.6.04-.6-.506V3.978c0-.53.482-.522.626-.506A5.132 5.132 0 0113.15 8.58c0 2.63-2.145 4.805-4.583 5.105z" fill="#73A7F0"/></svg>
                <?php
                $CourseFormat = get_field_object( 'page-course-courses-format' );
                $ValueCourseFormat = $CourseFormat['value'];
                $LabelCourseFormat = $CourseFormat['choices'][ $ValueCourseFormat ];
                ?>
                <?php echo $LabelCourseFormat?>
              </div>
            </div>
            <div class="cic-row">
              <div class="cic-text">
                <p class="cic-title"><?php echo the_field("page-course-block1-title")?></p>
                <ul>
                  <li>теория о типах кожи, их особенностях и выборе пигментов;</li>
                  <li>работа с инструментами разных производителей – израильских, немецких;</li>
                  <li>как избежать ошибок мастера; практика на моделях.</li>
                </ul>
              </div>
              <div class="cic-btns">
                <a href="<?php the_permalink(); ?>" class="btn-cic-sign">Записаться</a>
                <a href="<?php echo the_field("1724");?>" class="cic-timetable">Расписание <svg width="13.66" height="13.21" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 15"><path d="M14.124 7.051L7.114.967A.298.298 0 006.92.895H5.277c-.137 0-.2.17-.096.26l6.497 5.64H.82a.149.149 0 00-.148.148v1.114c0 .081.067.148.148.148h10.857l-6.498 5.64c-.104.092-.041.26.096.26h1.698c.035 0 .07-.013.097-.037l7.054-6.119a.594.594 0 000-.898z" fill="#73A7F0"/></svg></a>
                <?php
                $CourseCity = get_field_object( 'page-course-block1-city' );
                $ValueCourseCity = $CourseCity['value'];
                $LabelCourseCity = $CourseCity['choices'][ $ValueCourseCity ];
                ?>
                <div class="cic-location"><?php echo $LabelCourseCity?></div>
              </div>
            </div>
          </div>
        </div>
        <? endwhile;?>
        <? wp_reset_query(); ?>
        <div class="courses-more">
          Показать ещё
          <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15"><path d="M14.124 7.051L7.114.967A.298.298 0 006.92.895H5.277c-.137 0-.2.17-.096.26l6.497 5.64H.82a.149.149 0 00-.148.148v1.114c0 .081.067.148.148.148h10.857l-6.498 5.64c-.104.092-.041.26.096.26h1.698c.035 0 .07-.013.097-.037l7.054-6.119a.594.594 0 000-.898z" fill="#73A7F0"/></svg>
        </div>
        <hr>
      </div>
    </div>

  </main>


<?php
get_footer();