<?php
/**
 * Template file for footer
 *
 * LICENSE:
 *
 * @category   Zend
 * @package    Zend_Footer
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    $Id:$
 * @link       http://framework.zend.com/package/PackageName
 * @since      File available since Release 1.5.0
 */
?>


<footer>
  <div class="container">
    <div class="row justify-content-between">
      <div class="col-12 col-lg-4">
        <a href="#" class="logo"><img src="<?php echo the_field('footer-logo', 'option'); ?>"></a>
        <p class="copy"><?php echo the_field("footer-copy", "option")?></>
      </div>
      <div class="col-12 col-md-auto">
        <p class="title"><?php echo the_field("footer-title-about", "option")?></p>
        <?php
          wp_nav_menu( array(
              'container'       => 'false',
              'menu'            => 'Footer menu (About)',
              'echo'            => true,
              'fallback_cb'     => 'wp_page_menu',
              'items_wrap'      => '<ul class = "%2$s">%3$s</ul>',
          ) );
        ?>
      </div>
      <div class="col-12 col-md-auto">
        <p class="title"><?php echo the_field("footer-title-courses", "option")?></p>
        <?php
        wp_nav_menu( array(
            'container'       => 'false',
            'menu'            => 'Footer menu (Courses)',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'items_wrap'      => '<ul class = "%2$s">%3$s</ul>',
        ) );
        ?>
        <a href="<?php echo get_page_link(2236)?>" class="btn-all-course">
          <?php echo the_field("footer-btn-courses", "option")?>
          <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16"><path d="M13.578 7.622L7.675 2.498a.25.25 0 00-.164-.06H6.128a.124.124 0 00-.081.218l5.472 4.75H2.375a.125.125 0 00-.125.125v.938c0 .069.056.125.125.125h9.142l-5.472 4.75a.124.124 0 00.082.219h1.43c.029 0 .059-.011.08-.032l5.941-5.153a.5.5 0 000-.756z" fill="#000"/></svg>
        </a>
      </div>
      <div class="col-12 col-md-auto">
        <p class="title"><?php echo the_field("footer-title-contacts", "option")?></p>
        <ul>
          <li><?php echo the_field("footer-kiev", "option")?></li>
          <li><a href="tel:<?php echo the_field("footer-kiev-phone-1-link", "option")?>"><?php echo the_field("footer-kiev-phone-1", "option")?></a></li>
          <li><a href="tel:<?php echo the_field("footer-kiev-phone-2-link", "option")?>"><?php echo the_field("footer-kiev-phone-2", "option")?></a></li>
          <li><a href="mailto:<?php echo the_field("footer-kiev-email", "option")?>"><?php echo the_field("footer-kiev-email", "option")?></a></li>
        </ul>
        <ul class="d-block d-md-none">
          <li><?php echo the_field("footer-odessa", "option")?></li>
          <li><a href="tel:<?php echo the_field("footer-odessa-phone-1-link", "option")?>"><?php echo the_field("footer-odessa-phone-1", "option")?></a></li>
          <li><a href="tel:<?php echo the_field("footer-odessa-phone-2-link", "option")?>"><?php echo the_field("footer-odessa-phone-2", "option")?></a></li>
          <li><a href="mailto:<?php echo the_field("footer-odessa-email", "option")?>"><?php echo the_field("footer-odessa-email", "option")?></a></li>
        </ul>
        <a target="_blank" href="mailto:<?php echo the_field("footer-writeus-email", "option")?>" class="btn-write">
          <?php echo the_field("footer-writeus", "option")?>
          <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15"><path d="M14.124 7.051L7.114.967A.298.298 0 006.92.895H5.277c-.137 0-.2.17-.096.26l6.497 5.64H.82a.149.149 0 00-.148.148v1.114c0 .081.067.148.148.148h10.857l-6.498 5.64c-.104.092-.041.26.096.26h1.698c.035 0 .07-.013.097-.037l7.054-6.119a.594.594 0 000-.898z" fill="#73A7F0"/></svg>
        </a>
        <div class="soc-row">
          <a href="<?php echo the_field("footer-facebook", "option")?>"><svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><circle cx="20" cy="20" r="17" fill="#fff"/><path d="M20 .8C9.396.8.8 9.396.8 20S9.396 39.2 20 39.2 39.2 30.604 39.2 20 30.604.8 20 .8zm4.548 13.268h-2.886c-.342 0-.722.45-.722 1.048V17.2h3.61l-.546 2.972H20.94v8.922h-3.406v-8.922h-3.09V17.2h3.09v-1.748c0-2.508 1.74-4.546 4.128-4.546h2.886v3.162z" fill="#FFA4E5"/></svg></a>
          <a href="<?php echo the_field("footer-instagram", "option")?>"><svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><circle cx="20" cy="20" r="20" fill="#FFA4E5"/><path d="M20 12.987c-2.447 0-2.752.01-3.709.048-.962.043-1.612.179-2.186.382-.6.207-1.144.53-1.593.949-.46.409-.815.905-1.04 1.453-.223.524-.372 1.117-.42 1.994-.043.873-.052 1.151-.052 3.383 0 2.231.012 2.51.052 3.382.048.875.197 1.47.42 1.994a4.014 4.014 0 001.04 1.453c.448.42.992.744 1.593.95.574.2 1.227.338 2.186.381.958.04 1.262.048 3.709.048s2.752-.01 3.709-.048c.959-.043 1.612-.182 2.186-.382.6-.207 1.144-.53 1.593-.949.46-.408.816-.904 1.04-1.453.22-.524.372-1.12.42-1.994.043-.873.052-1.151.052-3.382 0-2.232-.012-2.51-.052-3.383-.048-.875-.2-1.473-.42-1.994a4.014 4.014 0 00-1.04-1.453 4.403 4.403 0 00-1.593-.949c-.574-.203-1.227-.34-2.186-.382-.958-.04-1.262-.048-3.709-.048zm0 1.478c2.403 0 2.69.01 3.64.047.875.038 1.352.171 1.669.284a2.85 2.85 0 011.037.614c.299.265.528.588.67.944.125.289.271.724.312 1.523.04.866.052 1.128.052 3.32 0 2.19-.011 2.452-.055 3.318-.048.8-.194 1.234-.317 1.523-.17.386-.36.655-.675.946-.292.271-.646.48-1.037.612-.313.113-.797.246-1.676.284-.954.037-1.236.047-3.645.047s-2.69-.01-3.645-.05c-.876-.043-1.36-.177-1.676-.29a2.863 2.863 0 01-1.035-.614 2.456 2.456 0 01-.673-.947c-.126-.285-.27-.726-.317-1.528-.032-.86-.047-1.127-.047-3.314 0-2.186.015-2.453.047-3.325.047-.802.19-1.242.317-1.528.155-.39.357-.657.673-.946a2.72 2.72 0 011.035-.615c.317-.113.788-.246 1.667-.286.955-.031 1.236-.043 3.642-.043l.037.024zm0 2.518c-.607 0-1.208.108-1.768.32-.561.212-1.07.522-1.5.913a4.21 4.21 0 00-1.001 1.368 3.892 3.892 0 00-.351 1.613c0 .553.119 1.101.35 1.613.233.51.573.976 1.002 1.367.43.391.939.702 1.5.913.56.212 1.161.32 1.768.32s1.208-.108 1.768-.32a4.67 4.67 0 001.5-.913c.429-.392.769-.856 1.001-1.367a3.892 3.892 0 000-3.226 4.21 4.21 0 00-1.001-1.368 4.672 4.672 0 00-1.5-.913 5.008 5.008 0 00-1.768-.32zm0 6.95c-1.659 0-3-1.224-3-2.736 0-1.513 1.341-2.737 3-2.737s3 1.224 3 2.737c0 1.512-1.341 2.736-3 2.736zm5.887-7.119a.945.945 0 01-.317.697 1.137 1.137 0 01-.765.289 1.17 1.17 0 01-.413-.075 1.091 1.091 0 01-.35-.213.985.985 0 01-.235-.32.911.911 0 010-.753.986.986 0 01.235-.32c.1-.091.219-.164.35-.213a1.17 1.17 0 01.413-.075c.595 0 1.082.44 1.082.983z" fill="#fff"/></svg></a>
        </div>
      </div>
    </div>
  </div>
</footer>

<div class="modal-shadow"></div>

<div class="modal-wrap-login">
  <form class="login-body" name="loginform" id="loginform" action="<?php bloginfo('url')?>/wp-login.php" method="post">
    <svg class="btn-login-close" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26"><path d="M24.375 24.375L1.625 1.625m0 22.75l22.75-22.75-22.75 22.75z" stroke="#3E4145" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>
    <p class="modal-title"><?php echo the_field("form-login-title", "option") ?></p>
    <p class="modal-subtitle"><?php echo the_field("form-login-subtitle", "option")?></p>
    <input type="email" name="log" id="user_login" required="" placeholder="<?php echo the_field("form-login-email", "option")?>">
    <input type="password" name="pwd" id="user_pass" required="" placeholder="<?php echo the_field("form-login-password", "option")?>">
    <button class="btn-login-send" type="submit" name="wp-submit" id="wp-submit"><?php echo the_field("form-login-btn", "option")?></button>
    <input type="hidden" name="redirect_to" value="<?php bloginfo('url')?>/wp-admin/" />
    <input type="hidden" name="testcookie" value="1" />
    <p class="modal-underbtn"><?php echo the_field("form-login-preregister", "option")?> <span class="call-reg"><?php echo the_field("form-login-register", "option")?></span></p>
  </form>
</div>


<div class="modal-wrap-reg">
  <form class="reg-body">
    <svg class="btn-reg-close" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26"><path d="M24.375 24.375L1.625 1.625m0 22.75l22.75-22.75-22.75 22.75z" stroke="#3E4145" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>
    <p class="modal-title"><?php echo the_field("form-reg-title", "option")?></p>
    <p class="modal-subtitle"><?php echo the_field("form-reg-subtitle", "option")?></p>
    <input type="text" name="regName" required="" placeholder="<?php echo the_field("form-reg-fio", "option")?>">
    <input type="tel" name="regPhone" required="" placeholder="<?php echo the_field("form-reg-phone", "option")?>" id="regPhone">
    <input type="email" name="regMail" required="" placeholder="<?php echo the_field("form-reg-email", "option")?>">
    <input type="password" name="regPass" required="" placeholder="<?php echo the_field("form-reg-password", "option")?>">
    <input type="password" name="regPassRe" required="" placeholder="<?php echo the_field("form-reg-repassword", "option")?>">
    <button class="btn-reg-send" type="submit"><?php echo the_field("form-reg-btn", "option")?></button>
    <p class="modal-underbtn"><?php echo the_field("form-reg-prelogin", "option")?> <span class="call-login"><?php echo the_field("form-reg-login", "option")?></span></p>
  </form>
</div>

<div class="modal-wrap-sign">
  <div class="sign-body">
    <svg class="btn-sign-close" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26"><path d="M24.375 24.375L1.625 1.625m0 22.75l22.75-22.75-22.75 22.75z" stroke="#3E4145" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>
    <?php echo do_shortcode('[Form id="12"]')?>
  </div>
</div>

<div class="modal-wrap-lecture">
  <form class="lecture-body">
    <svg class="btn-lecture-close" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26"><path d="M24.375 24.375L1.625 1.625m0 22.75l22.75-22.75-22.75 22.75z" stroke="#3E4145" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>
    <p class="modal-title"><?php echo the_field("form-buylecture-title", "option")?></p>
    <p class="modal-middle-title"><?php echo the_field("form-buylecture-title2", "option")?></p>
    <p class="modal-subtitle"><?php echo the_field("form-buylecture-subtitle", "option")?></p>
    <input type="text" name="lectureName" required="" placeholder="<?php echo the_field("form-buylecture-fio", "option")?>">
    <input type="tel" name="lecturePhone" required="" placeholder="<?php echo the_field("form-buylecture-phone", "option")?>" id="lecturePhone">
    <input type="email" name="lectureMail" required="" placeholder="<?php echo the_field("form-buylecture-email", "option")?>">
    <button class="btn-lecture-send" type="submit"><?php echo the_field("form-buylecture-btn", "option")?></button>
  </form>
</div>

<div class="modal-wrap-vacancyAdd">
  <form class="vacancyAdd-body">
    <svg class="btn-vacancyAdd-close" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26"><path d="M24.375 24.375L1.625 1.625m0 22.75l22.75-22.75-22.75 22.75z" stroke="#3E4145" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>
    <p class="modal-title"><?php echo the_field("modal-vacancyAdd-title", "option")?></p>
    <p class="modal-subtitle"><?php echo the_field("modal-vacancyAdd-subtitle", "option")?></p>
    <a href="/" class="btn-vacancyAdd-send"><?php echo the_field("modal-vacancyAdd-btn", "option")?></a>
  </form>
</div>

<div class="modal-wrap-resumeAdd">
  <form class="resumeAdd-body">
    <svg class="btn-resumeAdd-close" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26"><path d="M24.375 24.375L1.625 1.625m0 22.75l22.75-22.75-22.75 22.75z" stroke="#3E4145" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>
    <p class="modal-title"><?php echo the_field("modal-resumeAdd-title", "option")?></p>
    <p class="modal-subtitle"><?php echo the_field("modal-resumeAdd-subtitle", "option")?></p>
    <a href="/" class="btn-resumeAdd-send"><?php echo the_field("modal-resumeAdd-btn", "option")?></a>
  </form>
</div>

<div class="modal-wrap-review">
  <form class="review-body">
    <svg class="btn-review-close" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26"><path d="M24.375 24.375L1.625 1.625m0 22.75l22.75-22.75-22.75 22.75z" stroke="#3E4145" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>
    <p class="modal-title"><?php echo the_field("form-review-title", "option")?></p>
    <input type="text" name="reviewName" required="" placeholder="<?php echo the_field("form-review-name", "option")?>">
    <input type="tel" name="reviewPhone" required="" placeholder="<?php echo the_field("form-review-phone", "option")?>" id="reviewPhone">
    <input type="email" name="reviewMail" required="" placeholder="<?php echo the_field("form-review-email", "option")?>">
    <select id="reviewCategory">
      <option selected disabled hidden><?php echo the_field("form-review-category", "option")?></option>
      <option>Категория 1</option>
      <option>Категория 2 </option>
    </select>
    <div class="review-rating">
      <input hidden type="radio" name="reviewRating" id="reviewRating5">
      <label for="reviewRating5"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512.002 512.002"><path d="M511.267 197.258a14.995 14.995 0 00-12.107-10.209l-158.723-23.065-70.985-143.827a14.998 14.998 0 00-26.901 0l-70.988 143.827-158.72 23.065a14.998 14.998 0 00-8.312 25.585l114.848 111.954-27.108 158.083a14.999 14.999 0 0021.763 15.812l141.967-74.638 141.961 74.637a15 15 0 0021.766-15.813l-27.117-158.081 114.861-111.955a14.994 14.994 0 003.795-15.375z"/></svg></label>
      <input hidden type="radio" name="reviewRating" id="reviewRating4">
      <label for="reviewRating4"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512.002 512.002"><path d="M511.267 197.258a14.995 14.995 0 00-12.107-10.209l-158.723-23.065-70.985-143.827a14.998 14.998 0 00-26.901 0l-70.988 143.827-158.72 23.065a14.998 14.998 0 00-8.312 25.585l114.848 111.954-27.108 158.083a14.999 14.999 0 0021.763 15.812l141.967-74.638 141.961 74.637a15 15 0 0021.766-15.813l-27.117-158.081 114.861-111.955a14.994 14.994 0 003.795-15.375z"/></svg></label>
      <input hidden type="radio" name="reviewRating" id="reviewRating3">
      <label for="reviewRating3"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512.002 512.002"><path d="M511.267 197.258a14.995 14.995 0 00-12.107-10.209l-158.723-23.065-70.985-143.827a14.998 14.998 0 00-26.901 0l-70.988 143.827-158.72 23.065a14.998 14.998 0 00-8.312 25.585l114.848 111.954-27.108 158.083a14.999 14.999 0 0021.763 15.812l141.967-74.638 141.961 74.637a15 15 0 0021.766-15.813l-27.117-158.081 114.861-111.955a14.994 14.994 0 003.795-15.375z"/></svg></label>
      <input hidden type="radio" name="reviewRating" id="reviewRating2">
      <label for="reviewRating2"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512.002 512.002"><path d="M511.267 197.258a14.995 14.995 0 00-12.107-10.209l-158.723-23.065-70.985-143.827a14.998 14.998 0 00-26.901 0l-70.988 143.827-158.72 23.065a14.998 14.998 0 00-8.312 25.585l114.848 111.954-27.108 158.083a14.999 14.999 0 0021.763 15.812l141.967-74.638 141.961 74.637a15 15 0 0021.766-15.813l-27.117-158.081 114.861-111.955a14.994 14.994 0 003.795-15.375z"/></svg></label>
      <input hidden type="radio" name="reviewRating" id="reviewRating1">
      <label for="reviewRating1"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512.002 512.002"><path d="M511.267 197.258a14.995 14.995 0 00-12.107-10.209l-158.723-23.065-70.985-143.827a14.998 14.998 0 00-26.901 0l-70.988 143.827-158.72 23.065a14.998 14.998 0 00-8.312 25.585l114.848 111.954-27.108 158.083a14.999 14.999 0 0021.763 15.812l141.967-74.638 141.961 74.637a15 15 0 0021.766-15.813l-27.117-158.081 114.861-111.955a14.994 14.994 0 003.795-15.375z"/></svg></label>
    </div>
    <textarea name="reviewText" rows="6" placeholder="<?php echo the_field("form-review-text", "option")?>"></textarea>
    <input type="text" placeholder="<?php echo the_field("form-review-fb", "option")?>">
    <input type="text" placeholder="<?php echo the_field("form-review-ins", "option")?>">
    <input type="text" placeholder="<?php echo the_field("form-review-video", "option")?>">
    <button class="btn-review-send" type="submit"><?php echo the_field("form-review-btn", "option")?></button>
  </form>
</div>

</body>
<?php wp_footer(); ?>

</html>