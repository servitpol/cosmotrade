<?php
/**
 * functions.php file
 *
 * LICENSE:
 *
 * @category   Zend
 * @package    Zend_Functions
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    $Id:$
 * @link       http://framework.zend.com/package/PackageName
 * @since      File available since Release 1.5.0
 */
?>

<?php
add_action('wp_enqueue_scripts', 'theme_styles_scripts');

function theme_styles_scripts(){
  wp_register_style('style', get_template_directory_uri().'/assets/css/style.css');
  wp_enqueue_style('style');
  wp_register_script('jquery-min', get_template_directory_uri() . '/assets/js/jquery.min.js','', '', 'true');
  wp_enqueue_script('jquery-min');
  wp_register_script('slick', get_template_directory_uri() . '/assets/js/slick.min.js','', '', 'true');
  wp_enqueue_script('slick');
  wp_register_script('bootstrap.bundle', get_template_directory_uri() . '/assets/js/bootstrap.bundle.min.js','', '', 'true');
  wp_enqueue_script('bootstrap.bundle');
  wp_register_script('bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js','', '', 'true');
  wp_enqueue_script('bootstrap');
  wp_register_script('maskedinput', get_template_directory_uri() . '/assets/js/maskedinput.js','', '', 'true');
  wp_enqueue_script('maskedinput');
  wp_register_script('jquery-ui', get_template_directory_uri() . '/assets/js/jquery-ui.min.js','', '', 'true');
  wp_enqueue_script('jquery-ui');
  wp_register_script('main', get_template_directory_uri() . '/assets/js/main.js','', '', 'true');
  wp_enqueue_script('main');
};

add_theme_support( 'post-thumbnails' );
add_theme_support('menus');
add_theme_support('customize-selective-refresh-widgets');

remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );
remove_filter('comment_text', 'wpautop');

if ( function_exists( 'acf_add_options_page' ) ) {
  acf_add_options_page( array(
      'page_title' => 'Настройки Космотрейд',
      'menu_title' => 'Настройки Космотрейд',
      'menu_slug'  => 'options_page'
  ) );
}

show_admin_bar(false);