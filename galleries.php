<?php
/**
 * Template file for gallery page
 *
 * LICENSE:
 *
 * @category   Zend
 * @package    Zend_PageGallery
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    $Id:$
 * @link       http://framework.zend.com/package/PackageName
 * @since      File available since Release 1.5.0
 */
?>

<?php
/**
 * Template Name: Galleries
 */
get_header(); ?>

  <div class="container">
    <h1><?php echo the_field("page-galleries-title")?></h1>
    <div class="row">
      <div class="col-12 col-md-6 col-lg-4">
        <a href="#" class="gallery-item">
          <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/gallery-img1.png">
          <div class="gallery-item-row">
            <p class="gallery-item-title">Наша бюти <br>бизнес-школа</p>
            <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 37 37"><path d="M31.4 17.626L17.747 5.778a.58.58 0 00-.379-.141h-3.198a.288.288 0 00-.188.506l12.654 10.984H5.492a.29.29 0 00-.289.289v2.168c0 .159.13.289.29.289h21.14L13.98 30.857a.287.287 0 00.188.506h3.306a.278.278 0 00.188-.072l13.737-11.917a1.158 1.158 0 000-1.748z" fill="#8EB8F3"/></svg>
          </div>
        </a>
      </div>
      <div class="col-12 col-md-6 col-lg-4">
        <a href="#" class="gallery-item">
          <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/gallery-img2.png">
          <div class="gallery-item-row">
            <p class="gallery-item-title">Работы преподавателей</p>
            <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 37 37"><path d="M31.4 17.626L17.747 5.778a.58.58 0 00-.379-.141h-3.198a.288.288 0 00-.188.506l12.654 10.984H5.492a.29.29 0 00-.289.289v2.168c0 .159.13.289.29.289h21.14L13.98 30.857a.287.287 0 00.188.506h3.306a.278.278 0 00.188-.072l13.737-11.917a1.158 1.158 0 000-1.748z" fill="#8EB8F3"/></svg>
          </div>
        </a>
      </div>
      <div class="col-12 col-md-6 col-lg-4">
        <a href="#" class="gallery-item">
          <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/gallery-img3.png">
          <div class="gallery-item-row">
            <p class="gallery-item-title">Школа косметологии</p>
            <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 37 37"><path d="M31.4 17.626L17.747 5.778a.58.58 0 00-.379-.141h-3.198a.288.288 0 00-.188.506l12.654 10.984H5.492a.29.29 0 00-.289.289v2.168c0 .159.13.289.29.289h21.14L13.98 30.857a.287.287 0 00.188.506h3.306a.278.278 0 00.188-.072l13.737-11.917a1.158 1.158 0 000-1.748z" fill="#8EB8F3"/></svg>
          </div>
        </a>
      </div>
      <div class="col-12 col-md-6 col-lg-4">
        <a href="#" class="gallery-item">
          <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/gallery-img4.png">
          <div class="gallery-item-row">
            <p class="gallery-item-title">Инъекционные методики</p>
            <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 37 37"><path d="M31.4 17.626L17.747 5.778a.58.58 0 00-.379-.141h-3.198a.288.288 0 00-.188.506l12.654 10.984H5.492a.29.29 0 00-.289.289v2.168c0 .159.13.289.29.289h21.14L13.98 30.857a.287.287 0 00.188.506h3.306a.278.278 0 00.188-.072l13.737-11.917a1.158 1.158 0 000-1.748z" fill="#8EB8F3"/></svg>
          </div>
        </a>
      </div>
      <div class="col-12 col-md-6 col-lg-4">
        <a href="#" class="gallery-item">
          <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/gallery-img5.png">
          <div class="gallery-item-row">
            <p class="gallery-item-title">Перманентный макияж</p>
            <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 37 37"><path d="M31.4 17.626L17.747 5.778a.58.58 0 00-.379-.141h-3.198a.288.288 0 00-.188.506l12.654 10.984H5.492a.29.29 0 00-.289.289v2.168c0 .159.13.289.29.289h21.14L13.98 30.857a.287.287 0 00.188.506h3.306a.278.278 0 00.188-.072l13.737-11.917a1.158 1.158 0 000-1.748z" fill="#8EB8F3"/></svg>
          </div>
        </a>
      </div>
      <div class="col-12 col-md-6 col-lg-4">
        <a href="#" class="gallery-item">
          <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/gallery-img6.png">
          <div class="gallery-item-row">
            <p class="gallery-item-title">Микроблейдинг</p>
            <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 37 37"><path d="M31.4 17.626L17.747 5.778a.58.58 0 00-.379-.141h-3.198a.288.288 0 00-.188.506l12.654 10.984H5.492a.29.29 0 00-.289.289v2.168c0 .159.13.289.29.289h21.14L13.98 30.857a.287.287 0 00.188.506h3.306a.278.278 0 00.188-.072l13.737-11.917a1.158 1.158 0 000-1.748z" fill="#8EB8F3"/></svg>
          </div>
        </a>
      </div>
      <div class="col-12 col-md-6 col-lg-4">
        <a href="#" class="gallery-item">
          <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/gallery-img7.png">
          <div class="gallery-item-row">
            <p class="gallery-item-title">Выпускники Космотрейд</p>
            <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 37 37"><path d="M31.4 17.626L17.747 5.778a.58.58 0 00-.379-.141h-3.198a.288.288 0 00-.188.506l12.654 10.984H5.492a.29.29 0 00-.289.289v2.168c0 .159.13.289.29.289h21.14L13.98 30.857a.287.287 0 00.188.506h3.306a.278.278 0 00.188-.072l13.737-11.917a1.158 1.158 0 000-1.748z" fill="#8EB8F3"/></svg>
          </div>
        </a>
      </div>
    </div>
    <hr class="gallery-hr">
  </div>

<?php
get_footer();