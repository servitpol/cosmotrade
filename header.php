<?php
/**
 * Template file for header
 *
 * LICENSE:
 *
 * @category   Zend
 * @package    Zend_Header
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    $Id:$
 * @link       http://framework.zend.com/package/PackageName
 * @since      File available since Release 1.5.0
 */
?>

<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <title><?php echo the_title();?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>


<div class="header-mob">
  <div class="container">
    <a href="tel:<?php echo the_field('header-recall', 'option');?>" class="phone-btn">
      <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15"><path d="M14.792 11.766l-2.115 1.964c-.312.3-.726.401-1.13.402-1.781-.05-3.466-.87-4.849-1.711-2.27-1.546-4.353-3.463-5.66-5.78C.535 5.67-.053 4.431.003 3.346c.005-.408.122-.808.43-1.071L2.547.296c.439-.35.864-.228 1.145.179l1.701 3.02c.18.358.076.741-.19.997l-.78.729a.344.344 0 00-.08.208c.3 1.082 1.205 2.08 2.004 2.767.8.687 1.66 1.617 2.775 1.837.138.036.307.049.405-.037l.907-.863c.312-.222.763-.33 1.097-.149h.016l3.069 1.696c.45.265.497.776.175 1.086z" fill="#73A7F0"/></svg>
    </a>
    <a href="/" class="logo"><img src="<?php echo the_field('header-logo', 'option');?>"></a>
    <svg class="btn-menu" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 16"><path stroke="#73A7F0" stroke-width="2" stroke-linecap="round" d="M25 1H6M25 8H1.333M25 15H10.667"/></svg>
    <svg class="btn-menu-close" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 17"><path d="M15.938 15.938L1.062 1.062m0 14.876L15.938 1.062 1.062 15.938z" stroke="#73A7F0" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>
  </div>
</div>

<header>
  <div class="container">
    <div class="header-info">
      <a href="/" class="logo"><img src="<?php echo the_field('header-logo', 'option');?>"></a>
      <a href="tel:<?php echo the_field('header-phone-1-link', 'option');?>" class="phone"><?php echo the_field('header-phone-1', 'option');?></a>
      <a href="tel:<?php echo the_field('header-phone-2-link', 'option');?>" class="phone"><?php echo the_field('header-phone-2', 'option');?></a>
      <div class="soc-recall">
        <a class="soc" href="viber://chat?number=%2B<?php echo the_field('header-viber', 'option');?>">
          <svg fill="none" xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22"><path d="M19.384 16.956c-.986.873-2.416 1.499-4.289 1.878a20.77 20.77 0 01-5.789.333L5.532 22v-3.541c-1.244-.387-2.216-.888-2.916-1.503-.83-.73-1.472-1.778-1.93-3.144a13.102 13.102 0 010-8.371c.458-1.367 1.104-2.418 1.94-3.155C3.464 1.549 4.66.984 6.219.59A19.453 19.453 0 0111 0c1.63 0 3.223.197 4.782.59 1.558.394 2.755.959 3.591 1.696.837.737 1.483 1.788 1.94 3.155a13.1 13.1 0 010 8.37c-.457 1.367-1.1 2.415-1.929 3.145zm-5.682-5.258l1.393.236c-.114-1.717-.775-3.184-1.983-4.4-1.208-1.217-2.67-1.882-4.385-1.996l.236 1.395A5.33 5.33 0 0112.137 8.5a5.357 5.357 0 011.565 3.198zM9.199 8.37l.257 1.545c.572.286.994.708 1.265 1.266l1.544.258a3.926 3.926 0 00-1.094-1.964A4.174 4.174 0 009.2 8.371zM6.904 9.637V8.263c0-.243-.089-.486-.268-.73-.178-.243-.368-.418-.568-.525-.2-.108-.35-.118-.45-.032l-.986 1.008c-.558.558-.64 1.428-.247 2.608.393 1.18 1.144 2.325 2.251 3.434 1.108 1.11 2.252 1.86 3.431 2.254 1.18.393 2.04.311 2.584-.247l1.008-1.009c.1-.085.096-.232-.01-.44a1.628 1.628 0 00-.526-.568c-.243-.172-.486-.258-.73-.258h-1.372l-.793.687c-.629-.172-1.412-.726-2.348-1.664-.936-.937-1.49-1.727-1.662-2.371l.686-.773zM8.277 2.77l.214 1.395a7.942 7.942 0 013.967 1.127 8.144 8.144 0 012.895 2.897 8.135 8.135 0 011.136 3.96l1.394.236a9.407 9.407 0 00-.761-3.734 9.659 9.659 0 00-2.048-3.07 9.65 9.65 0 00-3.066-2.05 9.383 9.383 0 00-3.731-.761z" fill="#7D5CEC"/></svg>
        </a>
        <a class="soc" href="https://api.whatsapp.com/send?phone=<?php echo the_field('header-whatsapp', 'option');?>">
          <svg fill="none" xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22"><path d="M11.688 20.625a10.22 10.22 0 01-5.006-1.29L0 22l2.664-6.682a10.221 10.221 0 01-1.289-5.005c0-1.404.272-2.74.816-4.007a10.392 10.392 0 012.203-3.287A10.392 10.392 0 017.68.816 10.05 10.05 0 0111.687 0c1.404 0 2.74.272 4.007.816a10.392 10.392 0 013.287 2.203c.924.923 1.658 2.02 2.203 3.287.544 1.267.816 2.603.816 4.006 0 1.404-.272 2.74-.816 4.007a10.391 10.391 0 01-2.203 3.287 10.391 10.391 0 01-3.287 2.203 10.049 10.049 0 01-4.007.816zm3.437-8.25H13.75l-.773.688c-.645-.172-1.436-.727-2.375-1.665-.938-.939-1.493-1.73-1.665-2.375l.688-.773V6.875c0-.243-.086-.487-.258-.73a1.63 1.63 0 00-.57-.527c-.207-.107-.354-.118-.44-.032l-1.01 1.01c-.558.558-.64 1.428-.246 2.61.393 1.182 1.145 2.328 2.255 3.438 1.11 1.11 2.256 1.861 3.438 2.255 1.182.394 2.052.312 2.61-.247l1.01-1.01c.086-.085.075-.232-.032-.44a1.63 1.63 0 00-.527-.57c-.243-.171-.487-.257-.73-.257z" fill="#41D351"/></svg>
        </a>
        <a target="_blank" href="mailto:<?php echo the_field('header-writeus-email', 'option');?>" class="recall"><?php echo the_field('header-writeus', 'option');?></a>
      </div>
      <button class="login call-login"><?php echo the_field('header-login', 'option');?></button>
      <div class="lang-change-wrap">
        <div class="lang-change deactivated">
          <div class="lc-holder"></div>
          <?php
          pll_the_languages(array(
              'dropdown' => 0,
              'show_names' => 1,
              'show_flags' => 0,
              'hide_if_empty' => 0,
          ));
          ?>
        </div>
      </div>
    </div>
    <hr>
    <div class="header-nav">

      <?php
      if(pll_current_language() == 'ru') {
        wp_nav_menu( array(
            'container'       => 'false',
            'menu'            => 'Header menu 1',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'items_wrap'      => '<ul class = "%2$s">%3$s</ul>',
        ) );
      } else if(pll_current_language() == 'en') {
        wp_nav_menu( array(
            'container'       => 'false',
            'menu'            => 'Header menu 1 (en)',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'items_wrap'      => '<ul class = "%2$s">%3$s</ul>',
        ) );
      } else if(pll_current_language() == 'uk') {
        wp_nav_menu( array(
            'container'       => 'false',
            'menu'            => 'Header menu 1 (uk)',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'items_wrap'      => '<ul class = "%2$s">%3$s</ul>',
        ) );
      }
      ?>

      <div class="lang-change-wrap">
        <div class="lang-change deactivated">
          <div class="lc-holder"></div>
          <?php
          pll_the_languages(array(
              'dropdown' => 0,
              'show_names' => 1,
              'show_flags' => 0,
              'hide_if_empty' => 0,
          ));
          ?>
        </div>
      </div>
    </div>
  </div>
</header>