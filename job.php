<?php
/**
 * Template file for vacancy page
 *
 * LICENSE:
 *
 * @category   Zend
 * @package    Zend_PageVacancy
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    $Id:$
 * @link       http://framework.zend.com/package/PackageName
 * @since      File available since Release 1.5.0
 */
?>

<?php
/**
 * Template Name: Job
 * Template Post Type: post
 */
get_header(); ?>

<div class="page-job">
  <div class="container">
    <a href="<?php echo get_page_link(599)?>" class="btn-job-back"><svg width="14" height="16.88" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 18"><path d="M.21 8.292L7.396.517a.273.273 0 01.2-.092h1.683c.14 0 .205.218.099.332L2.717 7.965h11.13c.085 0 .153.085.153.19v1.422c0 .105-.069.19-.152.19H2.718l6.662 7.208c.106.116.042.332-.1.332H7.54a.132.132 0 01-.098-.047L.21 9.44a.77.77 0 01-.155-.259.922.922 0 010-.63.77.77 0 01.155-.259z" fill="#000"/></svg>Все вакансии</a>
    <div class="job-wrap">
      <h1><?php echo the_field("page-vacancies-item-title")?></h1>
      <div class="job-stats-pay">
        <div class="job-stats">
          <p><?php echo the_field("page-vacancies-item-city")?></p>
          <p><?php echo the_field("page-vacancies-item-place")?></p>
          <p><?php echo the_field("page-vacancies-item-type")?></p>
        </div>
        <p class="job-pay"><?php echo the_field("page-vacancies-item-price")?></p>
      </div>
      <p class="text"><?php echo the_field("page-vacancies-item-addtext")?></p>
      <p class="title">Требования к кандидату: </p>
      <p class="text">
        <?php echo the_field("page-vacancies-item-requirements")?>
      </p>
      <p class="title">Условия работы:</p>
      <p class="text">
        <?php echo the_field("page-vacancies-item-terms")?>
      </p>
      <a href="<?php echo get_page_link(16)?>" class="btn-job-send">Отправить резюме</a>
    </div>
    <hr>
  </div>
</div>

<?php
get_footer();