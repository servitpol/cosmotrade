<?php
/**
 * Template file for agency page
 *
 * LICENSE:
 *
 * @category   Zend
 * @package    Zend_PageAgency
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    $Id:$
 * @link       http://framework.zend.com/package/PackageName
 * @since      File available since Release 1.5.0
 */
?>

<?php
/**
 * Template Name: Recruitment
 */
get_header(); ?>

<div class="page-agency">
  <div class="container">
    <div class="agency-heading">
      <div class="agency-heading-top">
        <div class="row">
          <div class="col-12 col-md-7">
            <h1><?php echo the_field("page-recruitment-main-title")?></h1>
            <p><?php echo the_field("page-recruitment-main-text")?></p>
          </div>
          <div class="col-12 col-md-5">

          </div>
        </div>
        <img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/xkadrove.png">
      </div>
      <div class="agency-heading-bottom">
        <div class="row">
          <div class="col-12 col-xl-6">
            <div class="ahb-item">
              <div class="row">
                <div class="col-12 col-md-6">
                  <p class="title"><?php echo the_field("page-recruitment-search-title")?></p>
                  <p class="text"><?php echo the_field("page-recruitment-search-text")?></p>
                </div>
                <div class="col-12 col-md-6">
                  <a href="<?php echo get_page_link("16")?>" class="btn-ahb-item"><?php echo the_field("page-recruitment-search-btn")?></a>
                  <a href="<?php echo get_page_link("599")?>" class="ahb-item-more"><?php echo the_field("page-recruitment-search-link")?> <svg height="19" width="19" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 19"><path d="M16.124 9.051l-7.01-6.084a.298.298 0 00-.195-.072H7.277c-.137 0-.2.17-.096.26l6.497 5.64H2.82a.149.149 0 00-.148.148v1.114c0 .081.067.148.148.148h10.857l-6.498 5.64c-.104.092-.041.26.096.26h1.698c.035 0 .07-.012.097-.037l7.054-6.119a.594.594 0 000-.898z" fill="#fff"/></svg></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-xl-6">
            <div class="ahb-item">
              <div class="row">
                <div class="col-12 col-md-6">
                  <p class="title"><?php echo the_field("page-recruitment-job-title")?></p>
                  <p class="text"><?php echo the_field("page-recruitment-job-text")?></p>
                </div>
                <div class="col-12 col-md-6">
                  <a href="<?php echo get_page_link("652")?>" class="btn-ahb-item"><?php echo the_field("page-recruitment-job-btn")?></a>
                  <a href="<?php echo get_page_link("599")?>" class="ahb-item-more"><?php echo the_field("page-recruitment-job-link")?> <svg height="19" width="19" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 19"><path d="M16.124 9.051l-7.01-6.084a.298.298 0 00-.195-.072H7.277c-.137 0-.2.17-.096.26l6.497 5.64H2.82a.149.149 0 00-.148.148v1.114c0 .081.067.148.148.148h10.857l-6.498 5.64c-.104.092-.041.26.096.26h1.698c.035 0 .07-.012.097-.037l7.054-6.119a.594.594 0 000-.898z" fill="#fff"/></svg></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="agency-text-block">
      <div class="row">
        <div class="col-12 col-md-6">
          <h2><?php echo the_field("page-recruitment-info1-title")?></h2>
          <p><?php echo the_field("page-recruitment-info1-text")?></p>
        </div>
        <div class="col-12 col-md-6">
          <img src="<?php echo the_field("page-recruitment-info1-img")?>">
        </div>
      </div>
      <div class="row flex-md-row-reverse">
        <div class="col-12 col-md-6">
          <h2><?php echo the_field("page-recruitment-info2-title")?></h2>
          <p><?php echo the_field("page-recruitment-info2-text")?></p>
        </div>
        <div class="col-12 col-md-6">
          <img src="<?php echo the_field("page-recruitment-info2-img")?>">
        </div>
      </div>
    </div>

    <div class="agency-can">
      <h2><?php echo the_field("page-recruitment-opportunities-title")?></h2>
      <div class="row">
        <div class="col-12 col-xl-4">
          <a href="<?php echo the_field("page-recruitment-opportunities-1-link")?>" class="agency-can-item">
            <img src="<?php echo the_field("page-recruitment-opportunities-1-ico")?>">
            <p class="title"><?php echo the_field("page-recruitment-opportunities-1-title")?></p>
            <p class="text"><?php echo the_field("page-recruitment-opportunities-1-text")?></p>
            <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 51 52"><path d="M43.219 24.97L24.533 8.752a.793.793 0 00-.519-.192h-4.377a.394.394 0 00-.257.692L36.7 24.287H7.757a.397.397 0 00-.396.396v2.968c0 .217.178.395.396.395h28.938l-17.32 15.035a.393.393 0 00.257.693h4.525a.38.38 0 00.258-.099l18.804-16.311a1.585 1.585 0 000-2.394z" fill="#66C3D0"/></svg>
          </a>
        </div>
        <div class="col-12 col-xl-4">
          <a href="<?php echo the_field("page-recruitment-opportunities-2-link")?>" class="agency-can-item">
            <img src="<?php echo the_field("page-recruitment-opportunities-2-ico")?>">
            <p class="title"><?php echo the_field("page-recruitment-opportunities-2-title")?></p>
            <p class="text"><?php echo the_field("page-recruitment-opportunities-2-text")?></p>
            <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 51 52"><path d="M43.219 24.97L24.533 8.752a.793.793 0 00-.519-.192h-4.377a.394.394 0 00-.257.692L36.7 24.287H7.757a.397.397 0 00-.396.396v2.968c0 .217.178.395.396.395h28.938l-17.32 15.035a.393.393 0 00.257.693h4.525a.38.38 0 00.258-.099l18.804-16.311a1.585 1.585 0 000-2.394z" fill="#66C3D0"/></svg>
          </a>
        </div>
        <div class="col-12 col-xl-4">
          <a href="<?php echo the_field("page-recruitment-opportunities-3-link")?>" class="agency-can-item">
            <img src="<?php echo the_field("page-recruitment-opportunities-3-ico")?>">
            <p class="title"><?php echo the_field("page-recruitment-opportunities-3-title")?></p>
            <p class="text"><?php echo the_field("page-recruitment-opportunities-3-text")?></p>
            <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 51 52"><path d="M43.219 24.97L24.533 8.752a.793.793 0 00-.519-.192h-4.377a.394.394 0 00-.257.692L36.7 24.287H7.757a.397.397 0 00-.396.396v2.968c0 .217.178.395.396.395h28.938l-17.32 15.035a.393.393 0 00.257.693h4.525a.38.38 0 00.258-.099l18.804-16.311a1.585 1.585 0 000-2.394z" fill="#66C3D0"/></svg>
          </a>
        </div>
      </div>
    </div>
    <hr>
  </div>
</div>

<?php
get_footer();