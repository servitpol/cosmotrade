<?php
/**
 * Template file for the page with the form "Add resume"
 *
 * LICENSE:
 *
 * @category   Zend
 * @package    Zend_PageAddResume
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    $Id:$
 * @link       http://framework.zend.com/package/PackageName
 * @since      File available since Release 1.5.0
 */
?>

<?php
/**
 * Template Name: Resume Add
 */
get_header(); ?>

  <div class="page-resume-add">
    <div class="container">
      <div class="btn-resume-add-back"><svg width="14" height="16.88" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 18"><path d="M.21 8.292L7.396.517a.273.273 0 01.2-.092h1.683c.14 0 .205.218.099.332L2.717 7.965h11.13c.085 0 .153.085.153.19v1.422c0 .105-.069.19-.152.19H2.718l6.662 7.208c.106.116.042.332-.1.332H7.54a.132.132 0 01-.098-.047L.21 9.44a.77.77 0 01-.155-.259.922.922 0 010-.63.77.77 0 01.155-.259z" fill="#000"/></svg>Назад</div>
      <div class="resume-add-wrap">
        <h1>Добавить резюме</h1>
        <p class="subtitle">Заполните анкету на нашем сайте или добавьте уже готовое</p>
        <div class="resume-tabs">
          <p class="active resumeWrite">Заполнить анкету</p>
          <p class="resumeUpload">Загрузить резюме</p>
        </div>
        <div class="resume-tab resume-tab-write active">
          <div class="row">
            <div class="col-12 col-md-6">
              <input type="text" required="" placeholder="ФИО">
            </div>
            <div class="col-12 col-md-6">
              <select id="resumeWriteBirth">
                <option selected disabled hidden>Год рождения</option>
                <option>1990</option>
                <option>1991</option>
                <option>1992</option>
                <option>1993</option>
              </select>
            </div>
            <div class="col-12 col-md-6">
              <input id="resumeWritePhone" type="phone" required="" placeholder="Телефон">
            </div>
            <div class="col-12 col-md-6">
              <select id="resumeWriteCity">
                <option selected disabled hidden>Город, в котором вы ищете работу</option>
                <option>Киев</option>
                <option>Одесса</option>
              </select>
            </div>
            <div class="col-12 col-md-6">
              <input type="mail" required="" placeholder="Электронная почта">
            </div>
            <div class="col-12 col-md-6">
              <input type="text" required="" placeholder="Желаемая должность">
            </div>
            <div class="col-12 col-md-6">
              <div class="form-title">
                <p>Образование</p>
                <div class="form-title-round">+</div>
              </div>
              <input type="text" placeholder="Учебное заведение">
              <input type="text" placeholder="Специальность">
              <div class="form-date-range">
                <p>С</p>
                <div class="fdr-wrap">
                  <select id="resumeWriteEducationMonthFrom">
                    <option selected disabled hidden>Месяц</option>
                    <option>Январь</option>
                    <option>Февраль</option>
                    <option>Март</option>
                  </select>
                  <select id="resumeWriteEducationYearFrom">
                    <option selected disabled hidden>Год</option>
                    <option>1990</option>
                    <option>1991</option>
                    <option>1992</option>
                    <option>1993</option>
                  </select>
                </div>
                <p>по</p>
                <div class="fdr-wrap">
                  <select id="resumeWriteEducationMonthTo">
                    <option selected disabled hidden>Месяц</option>
                    <option>Январь</option>
                    <option>Февраль</option>
                    <option>Март</option>
                  </select>
                  <select id="resumeWriteEducationYearTo">
                    <option selected disabled hidden>Год</option>
                    <option>1990</option>
                    <option>1991</option>
                    <option>1992</option>
                    <option>1993</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-6">
              <div class="form-title">
                <p>Опыт работы</p>
                <div class="form-title-round">+</div>
              </div>
              <input type="text" placeholder="Название компании">
              <input type="text" placeholder="Должность">
              <div class="form-date-range">
                <p>С</p>
                <div class="fdr-wrap">
                  <select id="resumeWriteWorkMonthFrom">
                    <option selected disabled hidden>Месяц</option>
                    <option>Январь</option>
                    <option>Февраль</option>
                    <option>Март</option>
                  </select>
                  <select id="resumeWriteWorkYearFrom">
                    <option selected disabled hidden>Год</option>
                    <option>1990</option>
                    <option>1991</option>
                    <option>1992</option>
                    <option>1993</option>
                  </select>
                </div>
                <p>по</p>
                <div class="fdr-wrap">
                  <select id="resumeWriteWorkMonthTo">
                    <option selected disabled hidden>Месяц</option>
                    <option>Январь</option>
                    <option>Февраль</option>
                    <option>Март</option>
                  </select>
                  <select id="resumeWriteWorkYearTo">
                    <option selected disabled hidden>Год</option>
                    <option>1990</option>
                    <option>1991</option>
                    <option>1992</option>
                    <option>1993</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12 col-md-6">
              <label class="label-title" for="resumeRequirements">Личные данные соискателя</label>
              <textarea name="resumeRequirements" id="resumeRequirements" placeholder="Личные данные о соискателе" rows="11"></textarea>
            </div>
            <div class="col-12 col-md-6">
              <label class="label-title" for="resumePhoto">Загрузите фотографию</label>
              <label class="label-area" for="resumePhoto">
                <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><path d="M6.25 8.333v-6.25h4.167v6.25h6.25V12.5h-6.25v6.25H6.25V12.5H0V8.333h6.25zm6.25 12.5v-6.25h6.25v-6.25h14.583l3.813 4.167h6.604a4.179 4.179 0 014.167 4.167v25a4.179 4.179 0 01-4.167 4.166H10.417a4.179 4.179 0 01-4.167-4.166V20.833h6.25zm14.583 18.75c5.75 0 10.417-4.666 10.417-10.416S32.833 18.75 27.083 18.75s-10.416 4.667-10.416 10.417 4.666 10.416 10.416 10.416zm-6.666-10.416a6.66 6.66 0 006.666 6.666 6.66 6.66 0 006.667-6.666 6.66 6.66 0 00-6.667-6.667 6.66 6.66 0 00-6.666 6.667z" fill="#fff"/></svg>
                <p>Выберите с галереи или перетащите сюда фотографию</p>
                <span id="resumePhotoHolder" class="resume-img-holder"></span>
              </label>
              <input onchange="resumeImg(this);" type="file" name="resumePhoto" id="resumePhoto" hidden>
            </div>
          </div>
          <div class="row">
            <div class="col-12 col-md-6">
              <button class="btn-resume-add-write-send call-resumeAdd">Добавить</button>
            </div>
          </div>
        </div>
        <div class="resume-tab resume-tab-upload">
          <div class="row">
            <div class="col-12 col-md-6">
              <input class="input-text" type="text" placeholder="ФИО">
              <input class="input-text" type="mail" placeholder="Электронная почта">
              <input class="input-text" id="resumeUploadPhone" type="phone" placeholder="Телефон">
              <button class="btn-resume-add-upload-send call-resumeAdd">Отправить</button>
              <div class="check-row">
                <input type="checkbox" id="resumeCheck1">
                <label for="resumeCheck1">Добавить сопроводительный текст</label>
              </div>
              <div class="check-row">
                <input type="checkbox" id="resumeCheck2">
                <label for="resumeCheck2">Разместить резюме на сайте</label>
              </div>
              <div class="check-row">
                <input type="checkbox" id="resumeCheck3">
                <label for="resumeCheck3">Отправлять мне похожие вакансии</label>
              </div>
            </div>
            <div class="col-12 col-md-6">
              <label class="label-title" for="resumeFile">Файл с резюме</label>
              <label class="label-area" for="resumeFile">
                <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31 40"><path d="M13.904 23.036H9.21a.343.343 0 00-.246.104.362.362 0 00-.101.253v2.143c0 .094.036.185.101.252a.343.343 0 00.246.105h4.693v4.821c0 .095.036.186.102.253a.342.342 0 00.245.104h2.086c.092 0 .18-.037.246-.104a.362.362 0 00.102-.253v-4.821h4.692c.092 0 .18-.038.246-.105a.362.362 0 00.102-.252v-2.143a.362.362 0 00-.102-.253.343.343 0 00-.246-.104h-4.692v-4.822a.362.362 0 00-.102-.252.343.343 0 00-.246-.105H14.25a.343.343 0 00-.245.105.362.362 0 00-.102.252v4.822zM30.18 10.03c.26.268.408.63.408 1.01v27.53c0 .79-.621 1.429-1.39 1.429H1.39C.621 40 0 39.362 0 38.571V1.43C0 .639.621 0 1.39 0h18.453c.37 0 .726.152.986.42l9.35 9.611zm-2.798 1.665l-8.178-8.401v8.401h8.178z" fill="#fff"/></svg>
                <p id="resumeFileName">Выберите из папки или перетащите сюда файл</p>
              </label>
              <input type="file" name="resumeFile" id="resumeFile" hidden>
            </div>
          </div>
        </div>
      </div>
      <hr>
    </div>
  </div>

<?php
get_footer();