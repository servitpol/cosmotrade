<?php
/**
 * Template file for blog post
 *
 * LICENSE:
 *
 * @category   Zend
 * @package    Zend_PagePost
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    $Id:$
 * @link       http://framework.zend.com/package/PackageName
 * @since      File available since Release 1.5.0
 */
?>

<?php
/**
 * Template Name: Article
 * Template Post Type: post
 */
get_header(); ?>

  <div class="container">
  <div class="page-article-wrap">
    <a href="<?php echo get_page_link(2399)?>" class="btn-article-back"><svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 18"><path d="M.21 8.292L7.396.517a.273.273 0 01.2-.092h1.683c.14 0 .205.218.099.332L2.717 7.965h11.13c.085 0 .153.085.153.19v1.422c0 .105-.069.19-.152.19H2.718l6.662 7.208c.106.116.042.332-.1.332H7.54a.132.132 0 01-.098-.047L.21 9.44a.77.77 0 01-.155-.259.922.922 0 010-.63.77.77 0 01.155-.259z" fill="#73A7F0"/></svg>Все статьи</a>
    <h1><?php echo the_field("page-article-title")?></h1>
    <img src="<?php echo the_field('page-article-topimg'); ?>">
    <p class="date"><?php the_date('j F Y'); ?></p>
    <p class="excerpt"><?php echo the_field("page-article-toptext")?></p>
    <article>
      <?php echo the_field("page-article-text")?>
    </article>
    <div class="tags-wrap">
      <?php
      $currentPostCategory = wp_get_post_categories( $post->ID, array('fields' => 'all') );
      foreach( $currentPostCategory as $cat ){
        echo '<p>'. $cat->name .'</p>';
      }
      ?>
    </div>
    <div class="share">
      Поделиться с друзьями
      <a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>" class="bt-fb"><svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 36 36"><g clip-path="url(#clip0)"><path d="M17.996.72C8.455.72.721 8.454.721 17.995c0 9.541 7.734 17.275 17.275 17.275 9.541 0 17.275-7.734 17.275-17.275C35.271 8.454 27.537.72 17.996.72zm4.092 11.938h-2.597c-.307 0-.65.405-.65.943v1.875h3.249l-.491 2.674h-2.757v8.027h-3.065V18.15h-2.78v-2.674h2.78v-1.573c0-2.257 1.566-4.09 3.714-4.09h2.597v2.845z" fill="#FFA4E5"/></g><defs><clipPath id="clip0"><path fill="#fff" d="M0 0h35.99v35.99H0z"/></clipPath></defs></svg></a>
    </div>
  </div>

  <div class="similar-article">
    <h2>Похожие статьи</h2>
    <div class="row">
      <?php
      $tags = wp_get_post_tags($post->ID);
      if ($tags) {
        $first_tag = $tags[0]->term_id;
        $args=array(
            'tag__in' => array($first_tag),
            'post__not_in' => array($post->ID),
            'posts_per_page'=>6,
            'caller_get_posts'=>1,
            'category_name'=>'blog',
        );
        $my_query = new WP_Query($args);
        if( $my_query->have_posts() ) {
          while ($my_query->have_posts()) : $my_query->the_post(); ?>
            <div class="col-12 col-sm-6 col-lg-4">
              <a href="<?php the_permalink(); ?>">
                <figure style="background: url(<?php echo get_the_post_thumbnail_url(); ?>) no-repeat center / cover;"></figure>
                <p class="date"><?php the_date('j F Y'); ?></p>
                <p class="title"><?php echo the_field("page-article-title")?></p>
                <p class="description"><?php the_excerpt(); ?></p>
              </a>
            </div>
          <?php
          endwhile;
        }
        wp_reset_query();
      }
      ?>
    </div>
  </div>
  <hr class="article-hr">

<?php
get_footer();