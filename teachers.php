<?php
/**
 * Template file for teachers list page
 *
 * LICENSE:
 *
 * @category   Zend
 * @package    Zend_PageTeachers
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    $Id:$
 * @link       http://framework.zend.com/package/PackageName
 * @since      File available since Release 1.5.0
 */
?>

<?php
/**
 * Template Name: Teachers
 */
get_header(); ?>

  <main>

    <?php include("template-parts/content-courses.php");?>

    <div class="container">
      <h1><?php echo the_field("page-teachers-title")?></h1>
      <div class="courses-filter">
        <div class="courses-search">
          <input placeholder="Поиск по ключевым словам" type="text">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 515.558 515.558"><path d="M378.344 332.78c25.37-34.645 40.545-77.2 40.545-123.333C418.889 93.963 324.928.002 209.444.002S0 93.963 0 209.447s93.961 209.445 209.445 209.445c46.133 0 88.692-15.177 123.337-40.547l137.212 137.212 45.564-45.564L378.344 332.78zm-168.899 21.667c-79.958 0-145-65.042-145-145s65.042-145 145-145 145 65.042 145 145-65.043 145-145 145z"/></svg>
        </div>
      </div>
      <div class="courses-category">
        <div class="courses-category-item category-all active">Все курсы</div>
        <div class="courses-category-item cosmetology-teachers">Косметология</div>
        <div class="courses-category-item injection-techniques-teachers">Инъекционные методики</div>
        <div class="courses-category-item medical-teachers">Медицинские курсы</div>
        <div class="courses-category-item permanent-teachers">Перманентный макияж</div>
        <div class="courses-category-item trichology-teachers">Трихология</div>
        <div class="courses-category-item laser-hair-removal-teachers">Лазерная эпиляция</div>
        <div class="courses-category-item depilation-teachers">Депиляция</div>
        <div class="courses-category-item podology-teachers">Подология</div>
        <div class="courses-category-item business-teachers">Бизнес</div>
      </div>

      <div class="teachers-list">
        <div class="row">
        <? $args = array(
            'post_type' => 'post',
            'category_name'=>'teachers',
            'posts_per_page' => 12,
        );
        $loop = new wp_Query($args);
        while($loop->have_posts()) : $loop->the_post();?>
          <?php
          $categories = get_the_category();
          $cls = '';

          if ( ! empty( $categories ) ) {
            foreach ( $categories as $cat ) {
              $cls .= $cat->slug . ' ';
            }
          }
          ?>
          <div class="col-12 col-sm-6 col-md-4 teachers-item category-all show <?php echo $cls; ?>">
            <figure style="background: url(<?php echo the_field("page-teacher-img")?>) no-repeat center / cover;">
              <a href="<?php echo the_field("page-teacher-fb")?>" class="bt-fb"><svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><circle cx="20" cy="20" r="17" fill="#fff"/><path d="M20 .8C9.396.8.8 9.396.8 20S9.396 39.2 20 39.2 39.2 30.604 39.2 20 30.604.8 20 .8zm4.548 13.268h-2.886c-.342 0-.722.45-.722 1.048V17.2h3.61l-.546 2.972H20.94v8.922h-3.406v-8.922h-3.09V17.2h3.09v-1.748c0-2.508 1.74-4.546 4.128-4.546h2.886v3.162z" fill="#A9BAFF"/></svg></a>
              <a href="<?php echo the_field("page-teacher-ins")?>" class="bt-ig"><svg width="18" height="16.42" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18"><path d="M9 .987c-2.447 0-2.752.01-3.709.048-.961.043-1.612.179-2.186.382-.6.207-1.144.53-1.593.949-.46.409-.815.905-1.04 1.453C.248 4.343.1 4.936.051 5.813.01 6.687 0 6.964 0 9.196c0 2.231.012 2.51.052 3.382.048.875.197 1.47.42 1.994a4.013 4.013 0 001.04 1.453c.448.42.992.744 1.593.95.574.2 1.227.338 2.186.381.958.04 1.262.048 3.709.048s2.752-.01 3.709-.048c.959-.043 1.612-.182 2.186-.382.6-.207 1.144-.53 1.593-.949.46-.408.816-.904 1.04-1.453.22-.524.372-1.12.42-1.994.043-.873.052-1.151.052-3.382 0-2.232-.012-2.51-.052-3.383-.048-.875-.2-1.473-.42-1.994a4.013 4.013 0 00-1.04-1.453 4.4 4.4 0 00-1.593-.949c-.574-.203-1.227-.34-2.186-.382C11.751.995 11.447.987 9 .987zm0 1.478c2.403 0 2.69.01 3.64.047.875.038 1.352.171 1.669.284.392.132.746.342 1.037.614.299.265.528.588.67.944.125.289.271.724.312 1.523.04.866.052 1.128.052 3.32 0 2.19-.011 2.452-.055 3.318-.048.8-.194 1.234-.317 1.523-.17.386-.36.655-.675.946-.292.271-.646.48-1.037.612-.313.113-.797.246-1.676.284-.954.037-1.236.047-3.645.047s-2.69-.01-3.645-.05c-.876-.043-1.36-.177-1.676-.29a2.863 2.863 0 01-1.035-.614 2.456 2.456 0 01-.673-.947c-.126-.285-.27-.726-.317-1.528-.032-.86-.047-1.127-.047-3.314 0-2.186.015-2.453.047-3.325.047-.802.19-1.242.317-1.528.155-.39.357-.657.673-.946.314-.286.607-.47 1.035-.615.317-.113.788-.246 1.667-.286.955-.032 1.236-.043 3.642-.043L9 2.465zm0 2.518c-.607 0-1.208.108-1.768.32-.561.212-1.07.522-1.5.913a4.21 4.21 0 00-1.001 1.368 3.893 3.893 0 00-.351 1.613c0 .553.119 1.101.35 1.613a4.21 4.21 0 001.002 1.367c.43.391.939.702 1.5.913.56.212 1.161.32 1.768.32s1.208-.108 1.768-.32a4.67 4.67 0 001.5-.913c.429-.391.769-.856 1.001-1.367a3.892 3.892 0 000-3.226 4.209 4.209 0 00-1.001-1.368 4.671 4.671 0 00-1.5-.913A5.008 5.008 0 009 4.983zm0 6.95c-1.658 0-3-1.224-3-2.736C6 7.684 7.342 6.46 9 6.46c1.659 0 3 1.224 3 2.737 0 1.512-1.341 2.736-3 2.736zm5.887-7.119a.945.945 0 01-.318.697 1.137 1.137 0 01-.764.289 1.17 1.17 0 01-.413-.075 1.092 1.092 0 01-.35-.213.985.985 0 01-.235-.32.91.91 0 010-.754.985.985 0 01.234-.319c.1-.091.22-.164.35-.213a1.17 1.17 0 01.414-.075c.595 0 1.082.44 1.082.983z" fill="#fff"/></svg></a>
            </figure>
            <a href="<?php the_permalink(); ?>" class="name"><?php echo the_field("page-teacher-name")?></a>
            <p class="position"><?php echo the_field("page-teacher-speciality")?></p>
          </div>
        <? endwhile;?>
          <? wp_reset_query(); ?>
        </div>
        <div class="teacher-more">
          Показать ещё
          <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15"><path d="M14.124 7.051L7.114.967A.298.298 0 006.92.895H5.277c-.137 0-.2.17-.096.26l6.497 5.64H.82a.149.149 0 00-.148.148v1.114c0 .081.067.148.148.148h10.857l-6.498 5.64c-.104.092-.041.26.096.26h1.698c.035 0 .07-.013.097-.037l7.054-6.119a.594.594 0 000-.898z" fill="#73A7F0"/></svg>
        </div>
        <hr>
      </div>

    </div>

  </main>


<?php
get_footer();