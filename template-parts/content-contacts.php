<?php
/**
 * Template file for repeated contacts block
 *
 * LICENSE:
 *
 * @category   Zend
 * @package    Zend_BlockContacts
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    $Id:$
 * @link       http://framework.zend.com/package/PackageName
 * @since      File available since Release 1.5.0
 */
?>

<div class="block-map">
  <?php echo the_field("block-map-iframe", "option")?>
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-8 col-lg-6">
        <div class="block-map-inner">
          <h2><?php echo the_field("block-map-title", "option")?></h2>
          <div class="row">
            <div class="col-12 col-sm-6">
              <p class="city"><?php echo the_field("block-map-kiev-city", "option")?></p>
              <p class="location"><?php echo the_field("block-map-kiev-street", "option")?></p>
              <p class="phone"><a href="tel:<?php echo the_field("block-map-kiev-phone-1-link", "option")?>"><?php echo the_field("block-map-kiev-phone-1", "option")?></a></p>
              <p class="phone"><a href="tel:<?php echo the_field("block-map-kiev-phone-2-link", "option")?>"><?php echo the_field("block-map-kiev-phone-2", "option")?></a></p>
              <p class="mail"><a href="mailto:<?php echo the_field("block-map-kiev-email", "option")?>"><?php echo the_field("block-map-kiev-email", "option")?></a></p>
            </div>
            <div class="col-12 col-sm-6">
              <p class="city"><?php echo the_field("block-map-odessa-city", "option")?></p>
              <p class="location"><?php echo the_field("block-map-odessa-street", "option")?></p>
              <p class="phone"><a href="tel:<?php echo the_field("block-map-odessa-phone-1-link", "option")?>"><?php echo the_field("block-map-odessa-phone-1", "option")?></a></p>
              <p class="phone"><a href="tel:<?php echo the_field("block-map-odessa-phone-2-link", "option")?>"><?php echo the_field("block-map-odessa-phone-2", "option")?></a></p>
              <p class="mail"><a href="mailto:<?php echo the_field("block-map-odessa-email", "option")?>"><?php echo the_field("block-map-odessa-email", "option")?></a></p>
            </div>
          </div>
          <div class="soc-row">
            <div class="soc-title"><?php echo the_field("block-map-soc-title", "option")?></div>
            <a href="<?php echo the_field("block-map-soc-fb", "option")?>"><svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><circle cx="20" cy="20" r="17" fill="#fff"/><path d="M20 .8C9.396.8.8 9.396.8 20S9.396 39.2 20 39.2 39.2 30.604 39.2 20 30.604.8 20 .8zm4.548 13.268h-2.886c-.342 0-.722.45-.722 1.048V17.2h3.61l-.546 2.972H20.94v8.922h-3.406v-8.922h-3.09V17.2h3.09v-1.748c0-2.508 1.74-4.546 4.128-4.546h2.886v3.162z" fill="#FFA4E5"/></svg></a>
            <a href="<?php echo the_field("block-map-soc-ins", "option")?>"><svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><circle cx="20" cy="20" r="20" fill="#FFA4E5"/><path d="M20 12.987c-2.447 0-2.752.01-3.709.048-.962.043-1.612.179-2.186.382-.6.207-1.144.53-1.593.949-.46.409-.815.905-1.04 1.453-.223.524-.372 1.117-.42 1.994-.043.873-.052 1.151-.052 3.383 0 2.231.012 2.51.052 3.382.048.875.197 1.47.42 1.994a4.014 4.014 0 001.04 1.453c.448.42.992.744 1.593.95.574.2 1.227.338 2.186.381.958.04 1.262.048 3.709.048s2.752-.01 3.709-.048c.959-.043 1.612-.182 2.186-.382.6-.207 1.144-.53 1.593-.949.46-.408.816-.904 1.04-1.453.22-.524.372-1.12.42-1.994.043-.873.052-1.151.052-3.382 0-2.232-.012-2.51-.052-3.383-.048-.875-.2-1.473-.42-1.994a4.014 4.014 0 00-1.04-1.453 4.403 4.403 0 00-1.593-.949c-.574-.203-1.227-.34-2.186-.382-.958-.04-1.262-.048-3.709-.048zm0 1.478c2.403 0 2.69.01 3.64.047.875.038 1.352.171 1.669.284a2.85 2.85 0 011.037.614c.299.265.528.588.67.944.125.289.271.724.312 1.523.04.866.052 1.128.052 3.32 0 2.19-.011 2.452-.055 3.318-.048.8-.194 1.234-.317 1.523-.17.386-.36.655-.675.946-.292.271-.646.48-1.037.612-.313.113-.797.246-1.676.284-.954.037-1.236.047-3.645.047s-2.69-.01-3.645-.05c-.876-.043-1.36-.177-1.676-.29a2.863 2.863 0 01-1.035-.614 2.456 2.456 0 01-.673-.947c-.126-.285-.27-.726-.317-1.528-.032-.86-.047-1.127-.047-3.314 0-2.186.015-2.453.047-3.325.047-.802.19-1.242.317-1.528.155-.39.357-.657.673-.946a2.72 2.72 0 011.035-.615c.317-.113.788-.246 1.667-.286.955-.031 1.236-.043 3.642-.043l.037.024zm0 2.518c-.607 0-1.208.108-1.768.32-.561.212-1.07.522-1.5.913a4.21 4.21 0 00-1.001 1.368 3.892 3.892 0 00-.351 1.613c0 .553.119 1.101.35 1.613.233.51.573.976 1.002 1.367.43.391.939.702 1.5.913.56.212 1.161.32 1.768.32s1.208-.108 1.768-.32a4.67 4.67 0 001.5-.913c.429-.392.769-.856 1.001-1.367a3.892 3.892 0 000-3.226 4.21 4.21 0 00-1.001-1.368 4.672 4.672 0 00-1.5-.913 5.008 5.008 0 00-1.768-.32zm0 6.95c-1.659 0-3-1.224-3-2.736 0-1.513 1.341-2.737 3-2.737s3 1.224 3 2.737c0 1.512-1.341 2.736-3 2.736zm5.887-7.119a.945.945 0 01-.317.697 1.137 1.137 0 01-.765.289 1.17 1.17 0 01-.413-.075 1.091 1.091 0 01-.35-.213.985.985 0 01-.235-.32.911.911 0 010-.753.986.986 0 01.235-.32c.1-.091.219-.164.35-.213a1.17 1.17 0 01.413-.075c.595 0 1.082.44 1.082.983z" fill="#fff"/></svg></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>