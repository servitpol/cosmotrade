<?php
/**
 * Template file for repeated courses block
 *
 * LICENSE:
 *
 * @category   Zend
 * @package    Zend_BlockCoures
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    $Id:$
 * @link       http://framework.zend.com/package/PackageName
 * @since      File available since Release 1.5.0
 */
?>

<div class="block-courses">
  <div class="container">
    <div class="block-courses-inner">
      <div class="bci-content">
        <a href="#" class="bci-content-item">
          <p class="bci-ci-date">начало 13 марта</p>
          <p class="bci-ci-title">Курсы коррекции мимических морщин</p>
          <p class="bci-ci-info">(ботул-терапия)</p>
        </a>
        <a href="#" class="bci-content-item">
          <p class="bci-ci-date">начало 13 марта</p>
          <p class="bci-ci-title">Курсы коррекции мимических морщин</p>
          <p class="bci-ci-info">(ботул-терапия)</p>
        </a>
        <a href="#" class="bci-content-item">
          <p class="bci-ci-date">начало 13 марта</p>
          <p class="bci-ci-title">Курсы коррекции мимических морщин</p>
          <p class="bci-ci-info">(ботул-терапия)</p>
        </a>
        <a href="#" class="bci-content-item">
          <p class="bci-ci-date">начало 13 марта</p>
          <p class="bci-ci-title">Курсы коррекции мимических морщин</p>
          <p class="bci-ci-info">(ботул-терапия)</p>
        </a>
      </div>
    </div>
  </div>
</div>