<?php
/**
 * Template file for repeated "How do we teach" block
 *
 * LICENSE:
 *
 * @category   Zend
 * @package    Zend_BlockHowTeach
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    $Id:$
 * @link       http://framework.zend.com/package/PackageName
 * @since      File available since Release 1.5.0
 */
?>

<div class="block-who">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-6">
        <h2><?php echo the_field("block-who-title", "option")?></h2>
        <div class="who-slider-text">
          <div class="who-text">
            <p class="title">Всегда на связи — всегда поддержим</p>
            <p class="text">Пройти обучение может любой начинающий мастер и получить нужные навыки и умения для работы, для этого необходимо записаться на занятия в Киеве или в Одессе</p>
          </div>
          <div class="who-text">
            <p class="title">Всегда на связи — всегда поддержим</p>
            <p class="text">Пройти обучение может любой начинающий мастер и получить нужные навыки и умения для работы, для этого необходимо записаться на занятия в Киеве или в Одессе</p>
          </div>
        </div>
        <button class="btn-who"><?php echo the_field("block-who-btn", "option")?></button>
      </div>
      <div class="col-12 col-md-6">
        <div class="who-slider-img">
          <figure><img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/kisspng-handset-telephone-mobile-phones-stock-photography-free-premium-stock-photos-canva-5cbeef7d20bb132.png"></figure>
          <figure><img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/kisspng-handset-telephone-mobile-phones-stock-photography-free-premium-stock-photos-canva-5cbeef7d20bb132.png"></figure>
        </div>
      </div>
    </div>
  </div>
</div>