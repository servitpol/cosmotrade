<?php
/**
 * Template file for timetable page
 *
 * LICENSE:
 *
 * @category   Zend
 * @package    Zend_PageTimetable
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    $Id:$
 * @link       http://framework.zend.com/package/PackageName
 * @since      File available since Release 1.5.0
 */
?>

<?php
/**
 * Template Name: Timetable
 */
get_header(); ?>

<div class="page-timetable">
  <div class="container">
    <h1><?php echo the_field("page-timetable-title")?></h1>
    <div class="courses-filter">
      <div class="courses-select">
        <label for="coursesTime">Форма обучения</label>
        <select name="coursesTime" id="coursesTime">
          <option value="filter-any">Любая</option>
          <option value="filter-daily">Дневная</option>
          <option value="filter-evening">Вечерняя</option>
          <option value="filter-holiday">Выходного дня</option>
          <option value="filter-distance">Дистанционная</option>
        </select>
      </div>
    </div>
    <div class="courses-category">
      <div class="courses-category-item all active">Все курсы</div>
      <div class="courses-category-item cosmetology">Косметология</div>
      <div class="courses-category-item injection-techniques">Инъекционные методики</div>
      <div class="courses-category-item medical">Медицинские курсы</div>
      <div class="courses-category-item permanent">Перманентный макияж</div>
      <div class="courses-category-item trichology">Трихология</div>
      <div class="courses-category-item laser-hair-removal">Лазерная эпиляция</div>
      <div class="courses-category-item depilation">Депиляция</div>
      <div class="courses-category-item podology">Подология</div>
      <div class="courses-category-item business">Бизнес</div>
    </div>
    <hr>
  </div>
</div>

<?php
get_footer();