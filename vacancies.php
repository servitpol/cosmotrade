<?php
/**
 * Template file for vacancies list page
 *
 * LICENSE:
 *
 * @category   Zend
 * @package    Zend_PageVacancies
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    $Id:$
 * @link       http://framework.zend.com/package/PackageName
 * @since      File available since Release 1.5.0
 */
?>

<?php
/**
 * Template Name: Vacancies
 */
get_header(); ?>

<div class="page-vacancies">
  <div class="container">
    <h1><?php echo the_field("page-vacancies-title")?></h1>
    <? $args = array(
        'post_type' => 'post',
        'category_name'=>'vacancies',
        'posts_per_page' => 4,
    );
    $loop = new wp_Query($args);
    while($loop->have_posts()) : $loop->the_post();?>
    <div class="vacancies-item">
      <div class="row align-items-center">
        <div class="col-12 col-lg-9">
          <p class="title"><?php echo the_field("page-vacancies-item-title")?></p>
          <div class="stats">
            <p><?php echo the_field("page-vacancies-item-title")?></p>
            <p><?php echo the_field("page-vacancies-item-place")?></p>
          </div>
          <p class="text"><?php echo the_field("page-vacancies-item-addtext")?></p>
          <a href="<?php the_permalink(); ?>" class="btn-vacancies-more">Читать дальше<svg width="19" height="19" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 19"><path d="M16.124 9.051l-7.01-6.084a.298.298 0 00-.195-.072H7.277c-.137 0-.2.17-.096.26l6.497 5.64H2.82a.149.149 0 00-.148.148v1.114c0 .081.067.148.148.148h10.857l-6.498 5.64c-.104.092-.041.26.096.26h1.698c.035 0 .07-.012.097-.037l7.054-6.119a.594.594 0 000-.898z" fill="#73A7F0"/></svg></a>
        </div>
        <div class="col-12 col-lg-3">
          <p class="price"><?php echo the_field("page-vacancies-item-price")?></p>
          <a href="<?php echo get_page_link(16)?>" class="btn-vacancies-send">Отправить резюме</a>
        </div>
      </div>
    </div>
    <? endwhile;?>
    <? wp_reset_query(); ?>
    <div class="vacancies-more">
      Показать ещё
      <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15"><path d="M14.124 7.051L7.114.967A.298.298 0 006.92.895H5.277c-.137 0-.2.17-.096.26l6.497 5.64H.82a.149.149 0 00-.148.148v1.114c0 .081.067.148.148.148h10.857l-6.498 5.64c-.104.092-.041.26.096.26h1.698c.035 0 .07-.013.097-.037l7.054-6.119a.594.594 0 000-.898z" fill="#73A7F0"></path></svg>
    </div>
    <hr>
  </div>
</div>


<?php
get_footer();