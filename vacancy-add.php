<?php
/**
 * Template file for the page with the form "Add vacancy"
 *
 * LICENSE:
 *
 * @category   Zend
 * @package    Zend_PageAddVacancy
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    $Id:$
 * @link       http://framework.zend.com/package/PackageName
 * @since      File available since Release 1.5.0
 */
?>

<?php
/**
 * Template Name: Vacancy Add
 */
get_header(); ?>

  <div class="page-vacancy-add">
    <div class="container">
      <div class="btn-vacancy-add-back"><svg width="14" height="16.88" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 18"><path d="M.21 8.292L7.396.517a.273.273 0 01.2-.092h1.683c.14 0 .205.218.099.332L2.717 7.965h11.13c.085 0 .153.085.153.19v1.422c0 .105-.069.19-.152.19H2.718l6.662 7.208c.106.116.042.332-.1.332H7.54a.132.132 0 01-.098-.047L.21 9.44a.77.77 0 01-.155-.259.922.922 0 010-.63.77.77 0 01.155-.259z" fill="#000"/></svg>Назад</div>
      <div class="vacancy-add-wrap">
        <h1>Добавить вакансию</h1>
        <p class="subtitle">Опишите детали вакансии для соискателей</p>
        <div class="row">
          <div class="col-12 col-md-6">
            <input type="text" placeholder="Название вакансии" required="">
          </div>
          <div class="col-12 col-md-6">
            <select id="vacancyCity">
              <option selected disabled hidden>Город</option>
              <option>Киев</option>
              <option>Одесса</option>
            </select>
          </div>
          <div class="col-12 col-md-6">
            <input type="text" placeholder="Место работы (компания)" required="">
          </div>
          <div class="col-12 col-md-6">
            <select id="vacancyEmployment">
              <option selected disabled hidden>Занятость</option>
              <option>Полная</option>
              <option>Частичная</option>
            </select>
          </div>
          <div class="col-12 col-md-6">
            <div class="input-pay-wrap">
              <input name="vacancyPay" id="vacancyPay" type="text" placeholder="Заработная плата" required="">
              <label for="vacancyPay">грн.</label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-md-6">
            <label class="label-title" for="vacancyRequirements">Требования</label>
            <textarea name="vacancyRequirements" id="vacancyRequirements" placeholder="Личные данные соискателя" rows="11"></textarea>
          </div>
          <div class="col-12 col-md-6">
            <label class="label-title" for="vacancyConditions">Условия работы</label>
            <textarea name="vacancyConditions" id="vacancyConditions" placeholder="Опишите ваши условия работы" rows="11"></textarea>
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-md-6">
            <button class="btn-vacancy-add-send call-vacancyAdd">Добавить</button>
          </div>
        </div>
      </div>
      <hr>
    </div>
  </div>

<?php
get_footer();