<?php
/**
 * Template file for lecture catalog page
 *
 * LICENSE:
 *
 * @category   Zend
 * @package    Zend_PageVideos
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    $Id:$
 * @link       http://framework.zend.com/package/PackageName
 * @since      File available since Release 1.5.0
 */
?>

<?php
/**
 * Template Name: Videos
 */
get_header(); ?>

  <main>

    <?php include("template-parts/content-courses.php");?>

  <div class="container">
    <h1><?php echo the_field("page-lectures-title")?></h1>
    <div class="courses-filter">
      <div class="courses-search">
        <input placeholder="Поиск по ключевым словам" type="text">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 515.558 515.558"><path d="M378.344 332.78c25.37-34.645 40.545-77.2 40.545-123.333C418.889 93.963 324.928.002 209.444.002S0 93.963 0 209.447s93.961 209.445 209.445 209.445c46.133 0 88.692-15.177 123.337-40.547l137.212 137.212 45.564-45.564L378.344 332.78zm-168.899 21.667c-79.958 0-145-65.042-145-145s65.042-145 145-145 145 65.042 145 145-65.043 145-145 145z"/></svg>
      </div>
    </div>
    <div class="courses-category">
      <div class="courses-category-item lecture-all active">Все курсы</div>
      <div class="courses-category-item cosmetology-lecture">Косметология</div>
      <div class="courses-category-item injection-techniques-lecture">Инъекционные методики</div>
      <div class="courses-category-item medical-lecture">Медицинские курсы</div>
      <div class="courses-category-item permanent-lecture">Перманентный макияж</div>
      <div class="courses-category-item trichology-lecture">Трихология</div>
      <div class="courses-category-item laser-hair-removal-lecture">Лазерная эпиляция</div>
      <div class="courses-category-item depilation-lecture">Депиляция</div>
      <div class="courses-category-item podology-lecture">Подология</div>
      <div class="courses-category-item business-lecture">Бизнес</div>
    </div>

    <div class="video-list">
      <div class="row">
        <? $args = array(
            'post_type' => 'post',
            'category_name'=>'lecture',
            'posts_per_page' => 12,
        );
        $loop = new wp_Query($args);
        while($loop->have_posts()) : $loop->the_post();?>
          <?php
          $categories = get_the_category();
          $cls = '';

          if ( ! empty( $categories ) ) {
            foreach ( $categories as $cat ) {
              $cls .= $cat->slug . ' ';
            }
          }
          ?>
        <div class="col-12 col-sm-6 col-lg-4 <?php echo $cls; ?> lecture-item lecture-all show <?php echo the_field('page-lecture-page-sale'); ?>">
          <a href="<?php the_permalink(); ?>">
              <figure style="background: url(<?php echo the_field('page-lecture-page-img'); ?>) no-repeat center / cover;">
              <div class="sale-label"><svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21 17"><path d="M4.857 8.96c-1.203 0-2.185-.381-2.948-1.144C1.146 7.053.765 6.005.765 4.67c0-.895.176-1.665.528-2.31A3.606 3.606 0 012.745.886C3.375.549 4.08.38 4.857.38c.777 0 1.474.169 2.09.506A3.57 3.57 0 018.421 2.36c.352.645.528 1.415.528 2.31 0 1.335-.381 2.383-1.144 3.146-.763.763-1.745 1.144-2.948 1.144zM13.9.6h3.718L7.101 16H3.383L13.9.6zM4.857 6.584c.279 0 .506-.147.682-.44.19-.308.286-.8.286-1.474 0-.675-.095-1.159-.286-1.452-.176-.308-.403-.462-.682-.462s-.513.154-.704.462c-.176.293-.264.777-.264 1.452 0 .675.088 1.166.264 1.474.19.293.425.44.704.44zm11.286 9.636c-1.203 0-2.185-.381-2.948-1.144-.763-.763-1.144-1.811-1.144-3.146 0-.895.176-1.665.528-2.31a3.606 3.606 0 011.452-1.474c.63-.337 1.335-.506 2.112-.506.777 0 1.474.169 2.09.506.63.337 1.122.829 1.474 1.474.352.645.528 1.415.528 2.31 0 1.335-.381 2.383-1.144 3.146-.763.763-1.745 1.144-2.948 1.144zm0-2.376c.279 0 .506-.147.682-.44.19-.308.286-.8.286-1.474 0-.675-.095-1.159-.286-1.452-.176-.308-.403-.462-.682-.462s-.513.154-.704.462c-.176.293-.264.777-.264 1.452 0 .675.088 1.166.264 1.474.19.293.425.44.704.44z" fill="#fff" fill-opacity=".35"/></svg>Акция</div>
              <svg class="play" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 57 57"><path d="M28.5 57C12.76 57 0 44.24 0 28.5S12.76 0 28.5 0 57 12.76 57 28.5 44.24 57 28.5 57zm-7.125-42.729V42.73L42.75 28.5 21.375 14.271z" fill="#fff" fill-opacity=".56"/></svg>
            </figure>
            <div class="video-info-row">
              <div class="vir-time"><?php echo the_field('page-lecture-main-time'); ?></div>
              <div class="vir-price"><?php echo the_field('page-lecture-main-price'); ?></div>
              <div class="vir-price-old"><?php echo the_field('page-lecture-page-price'); ?></div>
            </div>
            <p class="title"><?php echo the_field('page-lecture-main-title'); ?></p>
            <p class="name"><?php echo the_field('page-lecture-main-teacher'); ?></p>
          </a>
        </div>
        <? endwhile;?>
        <? wp_reset_query(); ?>
      </div>
      <div class="video-more">
        Показать ещё
        <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15"><path d="M14.124 7.051L7.114.967A.298.298 0 006.92.895H5.277c-.137 0-.2.17-.096.26l6.497 5.64H.82a.149.149 0 00-.148.148v1.114c0 .081.067.148.148.148h10.857l-6.498 5.64c-.104.092-.041.26.096.26h1.698c.035 0 .07-.013.097-.037l7.054-6.119a.594.594 0 000-.898z" fill="#73A7F0"/></svg>
      </div>
      <hr>
    </div>

  </div>

  </main>


<?php
get_footer();