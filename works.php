<?php
/**
 * Template file for gallery item page
 *
 * LICENSE:
 *
 * @category   Zend
 * @package    Zend_PageWorks
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license   BSD License
 * @version    $Id:$
 * @link       http://framework.zend.com/package/PackageName
 * @since      File available since Release 1.5.0
 */
?>

<?php
/**
 * Template Name: Works
 */
get_header(); ?>

  <div class="page-works">
    <div class="container">
      <a href="<?php echo get_page_link(480)?>" class="btn-works-back"><svg width="14" height="16.88" fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 18"><path d="M.21 8.292L7.396.517a.273.273 0 01.2-.092h1.683c.14 0 .205.218.099.332L2.717 7.965h11.13c.085 0 .153.085.153.19v1.422c0 .105-.069.19-.152.19H2.718l6.662 7.208c.106.116.042.332-.1.332H7.54a.132.132 0 01-.098-.047L.21 9.44a.77.77 0 01-.155-.259.922.922 0 010-.63.77.77 0 01.155-.259z" fill="#000"/></svg><?php echo the_field("page-works-btn")?></a>
      <h1><?php echo the_field("page-works-title")?></h1>
      <p><?php echo the_field("page-works-text")?></p>
      <div class="row">
        <div class="col-6 col-md-4">
          <figure style="background: url(<?php echo get_bloginfo('template_url'); ?>/assets/img/works1.jpg) no-repeat center / cover;"></figure>
        </div>
        <div class="col-6 col-md-4">
          <figure style="background: url(<?php echo get_bloginfo('template_url'); ?>/assets/img/works2.jpg) no-repeat center / cover;"></figure>
        </div>
        <div class="col-6 col-md-4">
          <figure style="background: url(<?php echo get_bloginfo('template_url'); ?>/assets/img/works3.jpg) no-repeat center / cover;"></figure>
        </div>
        <div class="col-6 col-md-4">
          <figure style="background: url(<?php echo get_bloginfo('template_url'); ?>/assets/img/works1.jpg) no-repeat center / cover;"></figure>
        </div>
        <div class="col-6 col-md-4">
          <figure style="background: url(<?php echo get_bloginfo('template_url'); ?>/assets/img/works2.jpg) no-repeat center / cover;"></figure>
        </div>
        <div class="col-6 col-md-4">
          <figure style="background: url(<?php echo get_bloginfo('template_url'); ?>/assets/img/works3.jpg) no-repeat center / cover;"></figure>
        </div>
      </div>
      <div class="works-more">
        <?php echo the_field("page-works-more")?>
        <svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15 15"><path d="M14.124 7.051L7.114.967A.298.298 0 006.92.895H5.277c-.137 0-.2.17-.096.26l6.497 5.64H.82a.149.149 0 00-.148.148v1.114c0 .081.067.148.148.148h10.857l-6.498 5.64c-.104.092-.041.26.096.26h1.698c.035 0 .07-.013.097-.037l7.054-6.119a.594.594 0 000-.898z" fill="#73A7F0"></path></svg>
      </div>
      <hr>
    </div>
  </div>

<?php
get_footer();